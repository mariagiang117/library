// change color of save button
var oldPass = document.getElementById('old_pass');
var newPass = document.getElementById('new_pass');
var rePass = document.getElementById('re_pass');
var btnSave = document.querySelector('.btnSave');
oldPass.addEventListener('input', changeColor);
newPass.addEventListener('input', changeColor);
rePass.addEventListener('input', changeColor);
function changeColor() {
    if (oldPass.value !== '' && newPass.value !== '' && rePass.value !== '') {
        btnSave.classList.add('btnLogin_change');
        btnSave.removeAttribute('disabled');
    }else {
        btnSave.classList.remove('btnLogin_change'); 
        btnSave.setAttribute('disabled', true);
    }
}
