<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Category;
use App\Models\TitleBook;
use App\Models\Author;
use App\Http\Requests\RequestBook;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function loadBook()
    {
        $listBook = Book::query()->with('titleBook')->orderBy('id_book', 'DESC')->paginate(9);


        $listTitleBook = TitleBook::all();


        return view('book.loadBook', compact('listBook', 'listTitleBook'));
    }

    public function addBook()
    {
        $book = Book::orderBy('id_book', 'DESC')->first();

        $id_book_lastest = $book->id_book;

        $listTitleBook = TitleBook::all();

        return view('book.addBook', compact('id_book_lastest',  'listTitleBook'));
    }

    public function postAddBook(RequestBook $request)
    {
        // dd($request->all());
        Book::create([
            'id_book' => $request->idBook,
            'id_title_book' => $request->nameBook,
            'status' => 1,
        ]);
        $titleBook = TitleBook::find($request->nameBook);
        // dd($titleBook);
        $titleBook->update([
            'quantity' => $titleBook->quantity + 1,
        ]);
        $request->session()->flash('them', 'Thêm thành công');

        return redirect()->route('loadBook');
    }

    public function getUpdateBook(Book $id_book)
    {
        return view('book.updateBook', compact('id_book'));
    }

    public function postUpdateBook(RequestBook $request, Book $id_book)
    {
        // dd($request->all());
        $id_book->description = $request->descriptionBook;

        $id_book->status = $request->statusBook;

        $id_book->save();

        return redirect()->route('loadBook')->with('sua', 'Sửa thành công');
    }

    public function deleteBook(Book $id_book)
    {
        $id_book->delete();

        $titleBook = TitleBook::find($id_book->id_title_book);

        $titleBook->update([
            'quantity' => $titleBook->quantity - 1,
        ]);

        return redirect()->route('loadBook')->with('xoa', 'Xóa thành công');
    }
}
