<?php

namespace App\Http\Controllers;

use App\Jobs\MailExpireBook;
use App\Models\DetailCallCard;
use App\Models\Student;
use App\Models\Book;
use App\Models\PayCard;
use App\Models\TitleBook;
use Barryvdh\DomPDF\Facade\PDF;


class StatisticController extends Controller
{
    //thống kê các sách quá hạn trả
    public function expireBook()
    {
        $stt = 1;
        $student = Student::all();
        $listDetailCallCard = DetailCallCard::query()->where('status', 0)->with('callCard', 'book')->get();
        $listExpireBook = [];
        foreach ($listDetailCallCard as $detailCallCard) {
            $date_expire = $detailCallCard->callCard->deadline;
            $date_now = date('Y-m-d');
            if ($date_now > $date_expire) {
                $date = abs(strtotime($date_now) - strtotime($date_expire));
                $day = floor($date / (60 * 60 * 24));
                $detailCallCard['day_expire'] = $day;
                $listExpireBook[] = $detailCallCard;
            }
        }
        return view('statistic.expire-book', compact('listExpireBook', 'student', 'stt'));
    }

    //gửi mail cho những người quá hạn trả
    public function mailforExpire(DetailCallCard $id_detail)
    {
        $id_student = $id_detail->callCard->id_student;
        $name_student = Student::find($id_student)->name_student;
        $emai = Student::find($id_student)->email;

        $id_book = $id_detail->id_book;
        $name_book = Book::find($id_book)->titleBook->name_title_book;

        $deadline = $id_detail->callCard->deadline;
        $date_now = date('Y-m-d');
        $day = abs(strtotime($date_now) - strtotime($deadline));
        $date_expire = floor($day / (60 * 60 * 24));

        $emailJob = new MailExpireBook($emai, $name_student, $name_book, $date_expire);
        dispatch($emailJob);

        return redirect()->route('expireBook')->with('success', 'Gửi mail thành công');
    }

    //thống kê các sách đang mượn
    public function bookBorrowing()
    {
        $stt = 1;
        $titleBook = TitleBook::all();
        $book = Book::all();
        $student = Student::all();
        $listDetailCallCard = DetailCallCard::query()->where('status', 0)->with('callCard', 'book')->paginate(10);
        return view('statistic.book-borrowing', compact('listDetailCallCard', 'student', 'stt', 'book', 'titleBook'));
    }

    //thống kê các sách đã mượn 
    public function borrowedBook()
    {
        $stt = 1;
        $titleBook = TitleBook::all();
        $book = Book::all();
        $listDetailCallCard = DetailCallCard::groupBy('id_book')
            ->selectRaw('count(*) as total, id_book')->orderBy('total', 'desc')->paginate(10);

        return view('statistic.borrowed-book', compact('listDetailCallCard',  'stt', 'book', 'titleBook'));
    }

    //thống kê các sách bị mất, hỏng
    public function lostBook()
    {
        $stt = 1;
        $titleBook = TitleBook::all();
        $book = Book::all();
        $student = Student::all();
        $listPayCard = PayCard::query()->where(function ($query) {
            $query->where('status', 2)->orWhere('status', 3);
        })->with('callCard', 'book', 'penalize')->paginate(10);
        return view('statistic.lost-book', compact('listPayCard', 'stt', 'book', 'titleBook', 'student'));
    }
    //xuất file pdf ds sách mất, hỏng
    public function exportLostBook()
    {
        $listPayCard = PayCard::query()->where(function ($query) {
            $query->where('status', 2)->orWhere('status', 3);
        })->with('callCard', 'book', 'penalize')->get();
        $data = [];
        foreach ($listPayCard as $payCard) {
            $data[] = $payCard;
        }
        $stt = 1;
        $book = Book::all();
        $student = Student::all();
        $titleBook = TitleBook::all();
        $pdf = \PDF::loadView('pdf.export-lost-book', compact('data', 'book', 'student', 'titleBook', 'stt'));
        return $pdf->stream('ds-sach-mat-hong.pdf');
    }
}
