<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Forgot Password</title>

    <!-- Custom fonts for this template-->
    <link href="/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/css/sb-admin-2.css" rel="stylesheet">
    <link href="/css/profile/resetPassword.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
    @if (Session::has('error'))
        <div class="alert alert-danger">
            <ul>
                <li>{!! Session::get('error') !!}</li>
            </ul>
        </div>
    @endif

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-2">Quên mật khẩu?</h1>
                                        <p class="mb-4">Hãy nhập email của bạn và chúng tôi sẽ gửi link tới
                                            email đó để lấy lại mật khẩu.</p>
                                    </div>
                                    <form class="user" method="POST"
                                        action="{{ route('post_forgot_password') }}">
                                        <div class="form-group">
                                            @csrf
                                            <input type="email" class="form-control form-control-user" name="email"
                                                id="exampleInputEmail" aria-describedby="emailHelp"
                                                placeholder="Nhập email">
                                        </div>
                                        <button class="btn btn-secondary btn-user btn-block btnResetPassword" disabled>
                                            Lấy lại mật khẩu
                                        </button>
                                    </form>
                                    <hr>

                                    <div class="text-center">
                                        <a class="small" href="login">Bạn đã có tài khoản? Đăng nhập!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="/js/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="/js/sb-admin-2.min.js"></script>
    {{-- Script for btnResetPassword --}}
    <script>
        var email = document.getElementById('exampleInputEmail');
        var btnResetPassword = document.querySelector('.btnResetPassword');
        email.addEventListener('input', changeColor);

        function changeColor() {
            if (email.value !== '') {
                btnResetPassword.classList.add('btnResetPassword_changeColor');
                btnResetPassword.removeAttribute('disabled');
            } else {
                btnResetPassword.classList.remove('btnResetPassword_changeColor');
                btnResetPassword.setAttribute('disabled', true);
            }
        }
    </script>
</body>

</html>
