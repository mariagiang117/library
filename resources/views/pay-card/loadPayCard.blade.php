@extends('dashboard/admin')
@section('content')
    @if (session('them'))
        <div class="alert alert-success">
            {{ session('them') }}
        </div>
    @endif
    @if (session('sua'))
        <div class="alert alert-success ">
            {{ session('sua') }}
        </div>
    @endif
    @if (session('xoa'))
        <div class="alert alert-success">
            {{ session('xoa') }}
        </div>
    @endif
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Phiếu trả </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-6 "></div>
        <div class="col-sm-6 col-md-6 d-flex justify-content-end gap-3 mb-2">
            <button class="btn btn-primary" type="button"><i class="fas fa-plus-circle"><a
                        href="{{ route('getAddPayCard') }}" class="text-white text-decoration-none"><span
                            class="ml-2">Thêm</span></i></a></button>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-striped col-xs-6">
                <thead>
                    <tr style="text-align: center" class="table-active">
                        <th scope="col" class="col-md-2">Mã phiếu trả</th>
                        <th scope="col" class="col-md-2">Mã sách</th>
                        <th scope="col" class="col-md-2">Người mượn</th>
                        <th scope="col" class="col-md-2">Ngày trả</th>
                        <th scope="col" class="col-md-2">Hạn trả</th>
                        <th scope="col" class="col-md-1">Mô tả</th>
                        <th scope="col" class="col-md-2">Tình trạng</th>
                        <th scope="col">Xóa</th>
                        <th scope="col">Sửa</th>
                        <th scope="col" class="col-md-2">Bồi thường</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listPayCard as $payCard)
                        <tr style="text-align: center">
                            <td>{{ $payCard->id_pay_card }}</td>
                            <td>{{ $payCard->id_book }}_{{$list_book->find($payCard->id_book)->titleBook->name_title_book}}</td>
                            <td>{{ $payCard->callCard->id_student }}_{{ $student->find($payCard->callCard->id_student)->name_student }}
                            </td>
                            <td>{{ date('d-m-Y', strtotime($payCard->date_pay_card)) }}</td>
                            <td>{{ date('d-m-Y', strtotime($callCard->find($payCard->id_call_card)->deadline)) }}</td>
                            <td>{{ $payCard->description }}</td>
                            <td>{{ $payCard->status }}</td>
                            <td><a href="{{ route('deletePayCard', ['id_PayCard' => $payCard->id_pay_card]) }}"
                                    onclick="return confirm('Bạn có chắc chắn muốn phiếu trả sách này?');"><i
                                        class="fas fa-trash"><span class="ml-2"
                                            id="btnDelete">Xóa</span></i></a>
                            </td>
                            <td><a href="{{ route('getUpdatePayCard', ['id_PayCard' => $payCard->id_pay_card]) }}"><i
                                        class="fas fa-edit"><span class="ml-2">Sửa</span></i></a></td>
                            <td><a href="{{ route('getAddPenalize', ['id_PayCard' => $payCard->id_pay_card]) }}"><button
                                        type="button" class="btn btn-danger">-</button></a></td>
                        </tr>
                </tbody>
                @endforeach
            </table>

            {!! $listPayCard->links() !!}

        </div>
    @endsection
