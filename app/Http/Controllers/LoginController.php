<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\forgotPassRequest;
use App\Http\Requests\Profile\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Jobs\SendMailForgotPassword;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('profile.login');
    }
    public function postLogin(LoginRequest $request)
    {
        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ])) {
            $user = Auth::user();
            $permission = $user->is_admin;
            $student = $user->student;
            //dd($student);
            if ($permission == 0) {
                // return redirect()->route('trangchu');
                // dd($id_student);
                return redirect()->route('trangchu');
            } else return redirect()->route('admin.index');
        } else return redirect()->back()->with('error', 'Email hoặc mật khẩu không đúng');
    }
    public function getForgotPassword()
    {
        return view('profile.forgotPassword');
    }
    public function postForgotPassword(forgotPassRequest $request)
    {
        $email = $request->email;
        $user = User::where('email', $email)->first();
        if (!$user) {
            return redirect()->back()->with('error', 'Email chưa được đăng kí!');
        } else {
            $random = rand(1, 1000000000);
            $password = Hash::make($random);
            //sendMail
            $emailJob = new SendMailForgotPassword($random, $email);
            dispatch($emailJob); //đẩy vào queue
            //save new pass in DB
            $user->update(['password' => $password]);
            return redirect()->route('login')->with('success', 'Mật khẩu mới đã được gửi vào email của bạn. Vui lòng kiểm tra email để lấy mật khẩu mới');
        }
    }
    public function getProfile()
    {
        return view('profile.account');
    }
    
}
