@extends('dashboard/admin')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <form action="{{ route('postUpdateBook', ['id_book' => $id_book->id_book]) }}" method="POST">
        @csrf
        @method('PUT')
        <h5 class="modal-title mb-3">Sửa sách</h5>
        <div class="row">
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="idBook" style="font-weight:bold">Mã sách</label>
                    <input type="text" id="idBook" name="idBook" class="form-control"
                        value="{{ isset($id_book->id_book) ? $id_book->id_book : '' }}" placeholder="Nhập mã sách"
                        readonly>
                        
                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">

                    <label for="nameBook" style="font-weight:bold">Tên sách</label>
                    <input type="text" id="nameBook" name="nameBook" class="form-control" readonly
                        value="{{ isset($id_book->titleBook->name_title_book) ? $id_book->titleBook->name_title_book : '' }}">
                       
                </div>
            </div>
            {{-- <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="statusBook" style="font-weight:bold">Tình trạng</label>
                    <select id="statusBook" name="statusBook" class="form-select">
                        <option selected value="{{$id_book->status}}">{{ $id_book->status }}</option>
                        <option value="0">Đang mượn</option>
                        <option value="1">Có sẵn</option>
                        <option value="2">Bị mất</option>
                    </select>
                    @if ($errors->has('statusBook'))
                    <span class="invalidfeedback">
                        {{ $errors->first('statusBook') }}
                    </span>
                @endif
                </div>
            </div> --}}

            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="description" style="font-weight:bold">Mô tả</label>
                    <input type="text" id="description" name="descriptionBook" class="form-control"
                        placeholder="Nhập mô tả">
                </div>
            </div>

        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary "><a href="{{ route('loadBook') }}"
                    class="text-white text-decoration-none"> Đóng</a></button>
            <button type="submit" class="btn btn-primary">Lưu</button>
        </div>

    </form>
@endsection
