<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required',
            // 'email' => 'required|email:rfc,dns',
            'password' => 'required|min:6',
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'Vui lòng nhập email',
            // 'email.email' => "Vui lòng nhập đúng định dạng email",
            'password.required' => 'Vui lòng nhập password',
            'password.min' => 'Mật khẩu phải có ít nhất 6 ký tự',
        ];
    }
}
