<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShelfBook extends Model
{
    use HasFactory;
    protected $table = 'shelf_books';
    protected $primaryKey = 'id_shelf_book';
    protected $fillable = [
        'id_shelf_book',
        'name_shelf_book',
        'position_shelf_book',
    ];
    public $timestamps = false;
    public function titleBooks()
    {
        return $this->hasMany(TitleBook::class, 'id_shelf_book', 'id_shelf_book');
    }
}
