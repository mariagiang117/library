@extends('dashboard/admin')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0">Sửa phiếu bồi thường </div>
    </div>
    <form method="POST" action="{{ route('postUpdatePenalize', ['id_Penalize' => $id_Penalize->id]) }}">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="mb-3 row">
                <label for="id_penalize" class="col-sm-2 col-form-label" style="font-weight:bold">Mã phiếu bồi thường</label>
                <div class="col-sm-6 col-md-4">
                    <input type="text" name="id_penalize" class="form-control" placeholder="Nhập mã sách" id="id_penalize"
                        value="{{ $id_Penalize->id }}" readonly>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="id_pay_card" class="col-sm-2 col-form-label" style="font-weight:bold">Mã phiếu trả</label>
                <div class="col-sm-6 col-md-4">
                    <input type="text" name="id_pay_card" class="form-control" id="id_pay_card"
                        value="{{ $id_Penalize->id_pay_card }}" readonly>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="description" class="col-sm-2 col-form-label" style="font-weight:bold">Mô tả</label>
                <div class="col-sm-6 col-md-4">
                    <input type="text" name="description" class="form-control" id="description"
                        value="{{ $id_Penalize->description }}">
                    @if ($errors->has('description'))
                        <span class="invalidfeedback">
                            {{ $errors->first('description') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="status" class="col-sm-2 col-form-label" style="font-weight:bold">Tình trạng</label>
                <div class="col-sm-6 col-md-4">
                    <select class="form-select" id="status" name="status">
                        <option selected disabled>Chọn trạng thái</option>
                        <option value="0">Chưa bồi thường</option>
                        <option value="1">Đã bồi thường</option>
                    </select>
                    @if ($errors->has('status'))
                        <span class="invalidfeedback">
                            {{ $errors->first('status') }}
                        </span>
                    @endif
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger"><a href="{{ route('loadPenalize') }}"
                        class="text-white text-decoration-none">Hủy</button>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    @endsection
