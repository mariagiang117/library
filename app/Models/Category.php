<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'id_category';
    protected $fillable = [
        'id_category',
        'name_category',
    ];
    public $timestamps = false;
    public function titleBook()
    {
        return $this->hasMany(TitleBook::class, 'id_category', 'id_category');
    }
}
