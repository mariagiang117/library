@extends('dashboard/admin')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .invalidfeedback {
            display: block;
            font-size: 1rem;
            color: #e3342f;
        }

        /* .checkbox-menu li label {
                                display: block;
                                padding: 3px 10px;
                                clear: both;
                                font-weight: normal;
                                line-height: 1.42857143;
                                color: #333;
                                white-space: nowrap;
                                margin: 0;
                                transition: background-color .4s ease;
                            }

                            .checkbox-menu li input {
                                margin: 0px 5px;
                                top: 2px;
                                position: relative;
                            }

                            .checkbox-menu li.active label {
                                background-color: #cbcbff;
                                font-weight: bold;
                            }

                            .checkbox-menu li label:hover,
                            .checkbox-menu li label:focus {
                                background-color: #f5f5f5;
                            }

                            .checkbox-menu li.active label:hover,
                            .checkbox-menu li.active label:focus {
                                background-color: #b8b8ff;
                            } */

    </style>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Sửa đầu sách </div>
    </div>
    <form method="POST" action="{{ route('postUpdateTitleBook', ['id_TitleBook' => $id_TitleBook->id_title_book]) }}"
        enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="idTitle_Book" style="font-weight:bold">Mã đầu sách</label>
                    <input type="text" id="idTitle_Book" name="idTitleBook" placeholder="Nhập mã sách" class="form-control"
                        value="{{ $id_TitleBook->id_title_book }}" readonly>
                </div>
                @if ($errors->has('idTitleBook'))
                    <span class="invalidfeedback">
                        {{ $errors->first('idTitleBook') }}
                    </span>
                @endif
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="nameTitle_Book" style="font-weight:bold">Tên đầu sách</label>
                    <input type="text" id="nameTitle_Book" name="nameTitleBook"
                        value="{{ $id_TitleBook->name_title_book }}" class="form-control" placeholder="Nhập tên sách">
                    @if ($errors->has('nameTitleBook'))
                        <span class="invalidfeedback">
                            {{ $errors->first('nameTitleBook') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="page" style="font-weight:bold">Số trang</label>
                    <input type="text" id="page" name="pageNumber" value="{{ $id_TitleBook->page_number }}"
                        class="form-control" placeholder="Nhập số trang">
                    @if ($errors->has('pageNumber'))
                        <span class="invalidfeedback">
                            {{ $errors->first('pageNumber') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="category" style="font-weight:bold">Thể loại</label>
                    <select id="category" name="category" class="form-select">
                        <option value="{{ $id_TitleBook->id_category }}" selected>
                            {{ $id_TitleBook->category->name_category }}</option>
                        @foreach ($listCategory as $Category)
                            <option value="{{ $Category->id_category }}">{{ $Category->name_category }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('category'))
                        <div class="invalidfeedback">
                            {{ $errors->first('category') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="publishers" style="font-weight:bold">Nhà xuất bản</label>
                    <select id="publishers" name="publisher" class="form-select">
                        <option value="{{ $id_TitleBook->id_publisher }}" selected>
                            {{ $id_TitleBook->publisher->name_publisher }}</option>
                        @foreach ($listPublisher as $Publisher)
                            <option value="{{ $Publisher->id_publisher }}">{{ $Publisher->name_publisher }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('publisher'))
                        <span class="invalidfeedback">
                            {{ $errors->first('publisher') }}
                        </span>
                    @endif

                </div>
            </div>
            {{-- <div class="mb-3 row">
                <div class="col-sm-6 col-md-6">
                    <label for="publishers" style="font-weight:bold">Tác giả</label>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="border:0.1px solid grey" >
                            <span >Chọn tác giả</span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu checkbox-menu allow-focus" aria-labelledby="dropdownMenu1">
                            @foreach ($listAuthor as $Author)
                                <li>
                                    <label>
                                        <input type="checkbox"> {{ $Author->name_author }}
                                    </label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div> --}}
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="author" style="font-weight:bold">Tác giả </label>
                    <select id="author" name="author" class="form-select">
                        @foreach ($id_TitleBook->authors as $author)
                            <option value="{{ $author->id_author }}" selected>{{ $author->name_author }} </option>
                        @endforeach
                        @foreach ($listAuthor as $Author)
                            <option value="{{ $Author->id_author }}">{{ $Author->name_author }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('author'))
                        <span class="invalidfeedback">
                            {{ $errors->first('author') }}
                        </span>
                    @endif

                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="shelfBooks" style="font-weight:bold">Kệ sách</label>
                    <select id="shelfBooks" name="shelfBook" class="form-select">
                        <option value="{{ $id_TitleBook->id_shelf_book }}" selected>
                            {{ $id_TitleBook->shelfBook->name_shelf_book }}</option>
                        @foreach ($listShelfBook as $ShelfBook)
                            <option value="{{ $ShelfBook->id_shelf_book }}">{{ $ShelfBook->name_shelf_book }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('shelfBook'))
                        <span class="invalidfeedback">
                            {{ $errors->first('shelfBook') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="image" style="font-weight:bold">Chọn ảnh</label>
                    <input type="file" id="image" name="image" class="form-control">
                    <img src="/images/book/{{ $id_TitleBook->image }}" width="100px" height="100px">
                    {{-- @error('image')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror --}}
                    @if ($errors->has('image'))
                        <span class="invalidfeedback">
                            {{ $errors->first('image') }}
                        </span>
                    @endif
                </div>
            </div>
           
            <div class="modal-footer ">
                <button type="button" class="btn btn-secondary"><a href="{{ route('loadTitleBook') }}"
                        class="text-white text-decoration-none">Hủy</button>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div>
    </form>
@endsection
