<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<style>
    body {
        font-family: DejaVu Sans;
        font-size: 14px;
    }

    table {
        border-collapse: collapse;
    }

    table,
    th,
    td {
        border: 1px solid black;
    }

    .item0 {
        width: 25px;
    }

    .item1 {
        width: 85px;
    }

    .item2 {
        width: 140px;
    }

    .item3 {
        width: 100px;
    }

    .item4 {
        width: 150px;
    }

    .pagenum:before {
        content: counter(page);
    }

    .page-break {
        page-break-after: always;
    }

    footer {
        border-top: 1px solid black;
        position: fixed;
        bottom: -50px;
        left: 0px;
        right: 0px;
        height: 50px;
    }

</style>

<body>
    <header><span> <b> Thanh Liêm, {{ date('Y-m-d') }}</b></span></header>
    <main>
        <h1 style="text-align: center">Sách bị mất, hỏng</h1>
        <div>
            <table class="total">
                <thead>
                    <tr style="text-align: center" >
                        <th class="item0">STT</th>
                        <th class="item1">Mã sách</th>
                        <th class="item2">Tên sách</th>
                        <th class="item3">Tình trạng</th>
                        <th class="item4">Người làm hỏng</th>
                        <th class="item5">Bồi thường</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $value)
                        <tr style="text-align: center">
                            <td class="item0">{{ $stt++ }}</th>
                            <td class="item1">{{ $value->id_book }}</th>
                            <td class="item2">
                                {{ $titleBook->find($book->find($value->id_book)->id_title_book)->name_title_book }}
                                </th>
                            <td class="item3">{{ $value->status }}</th>
                            <td class="item4">
                                {{ $value->callCard->id_student }}_{{ $student->find($value->callCard->id_student)->name_student }}
                                </th>
                            <td>
                                @if ($value->penalize == null)
                                    {{ 'Chưa bồi thường' }}
                                @else
                                    {{ $value->penalize->description }}
                                @endif
                            </td>
                        </tr>
                </tbody>
                @endforeach
            </table>
    </main>
    <footer>
        <span style="float: right" class="pagenum"> </span>
    </footer>
    </div>
</body>

</html>
