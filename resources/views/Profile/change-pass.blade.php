<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Thay đổi mật khẩu</title>

    <!-- Custom fonts for this template-->
    <link href="/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/css/sb-admin-2.css" rel="stylesheet">
    <link href="/css/profile/login.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
    @if (Session::has('error'))
        <div class="alert alert-danger">
            {!! Session::get('error') !!}
        </div>
    @endif
    @if (Session::has('success'))
        <div class="alert alert-success">
            {!! Session::get('success') !!}
        </div>
    @endif

    <div class="container">
        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Thay đổi mật khẩu
                                        </h1>
                                    </div>
                                    <form class="user" method="POST"
                                        action="{{ route('postChangePassword') }}">
                                        @csrf
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="old_pass"
                                                placeholder="Nhập mật khẩu cũ" name="old_pass">

                                            {{-- @if ($errors->has('email'))
                                                <span class="invalidfeedback">
                                                    {{ $errors->first('email') }}
                                                </span>
                                            @endif --}}
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="new_pass"
                                                placeholder="Nhập mật khẩu mới" name="new_pass">
                                            {{-- @if ($errors->has('password'))
                                                <span class="invalidfeedback">
                                                    {{ $errors->first('password') }}
                                                </span>
                                            @endif --}}
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="re_pass"
                                                placeholder="Nhập lại mật khẩu" name="re_pass">
                                            {{-- @if ($errors->has('password'))
                                                <span class="invalidfeedback">
                                                    {{ $errors->first('password') }}
                                                </span>
                                            @endif --}}
                                        </div>
                                        <div style="text-align: right">
                                            <button type="submit" class="btn btn-secondary btn-user  btnSave" disabled>
                                                Lưu
                                            </button>
                                            <a href="{{ route('admin.index') }}"><button type="button"
                                                    class="btn btn-danger btn-user ">
                                                    Hủy
                                                </button></a>
                                        </div>
                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="/js/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="/js/sb-admin-2.min.js"></script>

    <!-- Change color button Login -->
    <script src="/js/profile/change-pass.js"></script>
</body>

</html>
