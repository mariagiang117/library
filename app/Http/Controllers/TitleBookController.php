<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestTitleBook;
use App\Http\Requests\RequestUpdateTitleBook;
use App\Models\Author;
use App\Models\AuthorTitleBook;
use App\Models\Book;
use App\Models\TitleBook;
use App\Models\Category;
use App\Models\Publisher;
use App\Models\ShelfBook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TitleBookController extends Controller
{
    public function loadTitleBook()
    {
        $listCategory = Category::all();
        $listAuthor = Author::all();
        $listPublisher = Publisher::all();
        $listShelfBook = ShelfBook::all();
        $listTitleBook = TitleBook::query()->orderBy('id_title_book', 'desc')
            ->with('category', 'shelfBook', 'publisher', 'authors')->paginate(9);
        return view(
            'title-book.titleBook',
            compact('listTitleBook', 'listCategory', 'listAuthor', 'listPublisher', 'listShelfBook')
        );
    }
    public function addTitleBook()
    {
        $titleBook = TitleBook::orderBy('id_title_book', 'DESC')->first();
        $id_titleBook_lastest = $titleBook->id_title_book + 1;
        $listCategory = Category::all();
        $listAuthor = Author::all();
        $listPublisher = Publisher::all();
        $listShelfBook = ShelfBook::all();
        return view('title-book.addTitleBook', compact('id_titleBook_lastest', 'listCategory', 'listAuthor', 'listPublisher', 'listShelfBook'));
    }
    public function postAddTitleBook(RequestTitleBook $request)
    {
        $name = $request->file('image')->getClientOriginalName();
        $image = $request->file('image');
        $image->move(public_path('/images/book'), $name);

        TitleBook::create([
            'id_title_book' => $request->idTitleBook,
            'name_title_book' => $request->nameTitleBook,
            'page_number' => $request->pageNumber,
            'id_category' => $request->category,
            'id_shelf_book' => $request->shelfBook,
            'id_publisher' => $request->publisher,
            'cost' => $request->cost,
            'image' => $name,
            'quantity' => '0',
        ]);
        AuthorTitleBook::create([
            'id_author' => $request->author,
            'id_title_book' => $request->idTitleBook,
        ]);
        return redirect()->route('loadTitleBook')->with('them', 'Thêm thành công');
    }
    public function updateTitleBook(TitleBook $id_TitleBook)
    {
        $listCategory = Category::all();
        $listAuthor = Author::all();
        $listPublisher = Publisher::all();
        $listShelfBook = ShelfBook::all();
        return view('title-book.updateTitleBook', compact('id_TitleBook', 'listCategory', 'listAuthor', 'listPublisher', 'listShelfBook'));
    }
    public function postUpdateTitleBook(TitleBook $id_TitleBook, RequestUpdateTitleBook $request)
    {
        // dd($request->all());
        if ($request->hasFile('image')) {
            $name = $request->file('image')->getClientOriginalName();
            $image = $request->file('image');
            $image->move(public_path('/images/book'), $name);
            $id_TitleBook->update([
                'name_title_book' => $request->nameTitleBook,
                'page_number' => $request->pageNumber,
                'id_category' => $request->category,
                'id_shelf_book' => $request->shelfBook,
                'id_publisher' => $request->publisher,
                'cost' => $request->cost,
                'image' => $name,
            ]);
        }
        // $id_TitleBook->update([
        //     'name_title_book' => $request->nameTitleBook,
        //     'page_number' => $request->pageNumber,
        //     'id_category' => $request->category,
        //     'id_shelf_book' => $request->shelfBook,
        //     'id_publisher' => $request->publisher,
        //     'id_author' => $request->author,
        //     'cost' => $request->cost,
        // ]);
        $author_titleBook = AuthorTitleBook::where('id_title_book', $id_TitleBook->id_title_book)->first();

        $author_titleBook->update([
            'id_author' => $request->author,
        ]);

        return redirect()->route('loadTitleBook')->with('sua', 'Sửa thành công');
    }
    public function deleteTitleBook(TitleBook $id_TitleBook)
    {
        $id_TitleBook->delete();
        return redirect()->route('loadTitleBook')->with('xoa', 'Xóa thành công');
    }
    //search
    public function searchTitleBook(Request $request)
    {
        if ($request->ajax()) {
            $id_author = (!empty($request->get('author'))) ? ($request->get('author')) : ('');
            $id_publisher = (!empty($request->get('publisher'))) ? ($request->get('publisher')) : ('');
            $id_shelf_book = (!empty($request->get('shelfBook'))) ? ($request->get('shelfBook')) : ('');
            $id_category = (!empty($request->get('category'))) ? ($request->get('category')) : ('');
            $name_title_book = (!empty($request->get('nameTitlebook'))) ? ($request->get('nameTitlebook')) : ('');
            $listTitleBook = DB::table('title_books')
                ->join('categories', 'title_books.id_category', '=', 'categories.id_category')
                ->join('shelf_books', 'title_books.id_shelf_book', '=', 'shelf_books.id_shelf_book')
                ->join('publishers', 'title_books.id_publisher', '=', 'publishers.id_publisher')
                ->join('author_titlebook', 'title_books.id_title_book', '=', 'author_titlebook.id_title_book')
                ->join('authors', 'author_titlebook.id_author', '=', 'authors.id_author')
                ->when($id_category, function ($query, $id_category) {
                    return $query->where('categories.id_category',  $id_category);
                })
                ->when($id_shelf_book, function ($query, $id_shelf_book) {
                    return $query->where('shelf_books.id_shelf_book',  $id_shelf_book);
                })
                ->when($id_publisher, function ($query, $id_publisher) {
                    return $query->where('publishers.id_publisher',  $id_publisher);
                })
                ->when($id_author, function ($query, $id_author) {
                    return $query->where('authors.id_author',  $id_author);
                })
                ->when($name_title_book, function ($query, $name_title_book) {
                    return $query->where('title_books.name_title_book', 'LIKE', '%' . $name_title_book . '%');
                })
                ->select(
                    'title_books.*',
                    'categories.name_category',
                    'shelf_books.name_shelf_book',
                    'publishers.name_publisher',
                    'authors.name_author'
                )
                ->paginate(5);
            return json_encode(
                view('title-book.viewFilter', compact('listTitleBook'))->render()
            );
        }
    }
}
