<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;
    protected $table = 'authors';
    protected $primaryKey = 'id_author';
    protected $fillable = [
        'id_author',
        'name_author',
    ];
    public $timestamps = false;
    public function titleBooks()
    {
        return $this->belongsToMany(TitleBook::class, 'author_titleBook', 'id_author', 'id_titleBook');
    }
}
