@extends('dashboard/admin')
@section('content')
@if (session('error'))
<div class="alert alert-danger">
    {{ session('error') }}
</div>
@endif
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0">Thêm phiếu mượn </div>
    </div>
    <form method="POST" action="{{ route('postAddCallCard') }}">
        @csrf
        <div class="row">
            <div class="mb-3 row">
                <label for="id_call_card" class="col-sm-2 col-form-label" style="font-weight:bold">Mã phiếu mượn</label>
                <div class="col-sm-6 col-md-3">
                    <input type="text" name="id_call_card" class="form-control" placeholder="Nhập mã sách" id="id_call_card"
                        value="{{ $id_callCard_lastest }}" readonly>
                </div>

            </div>

            <div class="mb-3 row">
                <label for="id_student" class="col-sm-2 col-form-label" style="font-weight:bold">Người mượn</label>
                <div class="col-sm-6 col-md-3 ">
                    <select class="form-select" id="id_student" name="id_student">
                        <option selected disabled>Chọn người mượn</option>
                        @foreach ($listStudent as $student)
                            <option value="{{ $student->id_student }}">{{ $student->name_student }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('id_student'))
                        <span class="invalidfeedback">
                            {{ $errors->first('id_student') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="date_call" class="col-sm-2 col-form-label" style="font-weight:bold">Ngày mượn</label>
                <div class="col-sm-6 col-md-3">
                    <input type="date" name="date_call" class="form-control" id="date_call">
                    @if ($errors->has('date_call'))
                        <span class="invalidfeedback">
                            {{ $errors->first('date_call') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="deadline" class="col-sm-2 col-form-label" style="font-weight:bold">Hạn trả</label>
                <div class="col-sm-6 col-md-3">
                    <input type="date" name="deadline" class="form-control" id="deadline">
                    @if ($errors->has('deadline'))
                        <span class="invalidfeedback">
                            {{ $errors->first('deadline') }}
                        </span>
                    @endif
                </div>
            </div>

            <div class="mb-3 row">
                <label for="selectBook" class="col-sm-2 col-form-label" style="font-weight:bold">Sách mượn</label>
                <div class="col-sm-6 col-md-3">
                    <select name="selectBook" id="selectBook" class="form-select">
                        <option selected disabled>Chọn sách mượn</option>
                        @foreach ($listCallBook as $book)
                            <option value="{{ $book->id_book }}">{{ $book->id_book }} -
                                {{ $book->titleBook->name_title_book }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table  table-striped col-xs-6 col-md-6">
                    <thead>
                        <tr class="table-active">
                            <th scope="col" class="col-md-1">Mã sách</th>
                            <th scope="col" class="col-md-3">Tên sách </th>
                            <th scope="col" class="col-md-2">Xóa</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">

                    </tbody>

                </table>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-danger"><a
                        href="{{ route('deleteListDetail', ['id_CallCard' => $id_callCard_lastest]) }}"
                        class="text-white text-decoration-none">Hủy</button></a>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    </form>
@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#selectBook").change(function() {
                var id_book = $(this).val();
                var id_callCard = $('#id_call_card').val();
                
                if (id_book != '' && id_callCard != '') {
                    $.ajax({
                        url: "{{ route('addDetailCallCard') }}",
                        method: "POST",
                        dataType: "json",
                        data: {
                            _token: '{{ csrf_token() }}',
                            id_book: id_book,
                            id_callCard: id_callCard,
                        },
                        success: function(data) {
                            $('#tbody').append(`<tr>
                            <td>${data.id_book}</td>
                            <td>${data.name_title_book}</td>
                            <td class="text-center">
                                <button class="btn btn-danger delete" type="button"
                                 value="${data.id_detail_call_card}" >Xóa</button> </td>
                           </tr> `);
                        }
                    })
                }
            });
            $('#tbody').on('click', '.delete', function() {
                var $element = $(this).parent().parent();
                var id_DetailCallCard = $(this).val();
                $.ajax({
                    url: "deleteDetailCallCard/" + id_DetailCallCard,
                    method: "GET",
                    dataType: "json",
                    data: {
                        _token: '{{ csrf_token() }}',
                    },
                    success: function(data) {
                        $element.remove();
                    }
                })
            })
        });
    </script>
@endsection
