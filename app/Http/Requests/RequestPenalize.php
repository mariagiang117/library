<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestPenalize extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_penalize' => 'required|numeric|unique:penalize,id',
            'id_pay_card' => 'required|numeric|unique:penalize,id_pay_card',
            'description' => 'required',
            'status' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'id_penalize.required' => 'Vui lòng nhập mã phiếu bồi thường',
            'id_penalize.numeric' => 'Mã phiếu bồi thường phải là số',
            'id_penalize.unique' => 'Mã phiếu bồi thường đã tồn tại',
            'id_pay_card.unique' => 'Cuốn sách đã bồi thường',
            'description.required' => 'Vui lòng nhập lý do bồi thường',
            'status.required' => 'Vui lòng chọn trạng thái',
        ];
    }
}
