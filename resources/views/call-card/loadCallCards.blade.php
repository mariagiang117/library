@extends('dashboard/admin')
@section('content')
    @if (session('them'))
        <div class="alert alert-success">
            {{ session('them') }}
        </div>
    @endif
    @if (session('sua'))
        <div class="alert alert-success ">
            {{ session('sua') }}
        </div>
    @endif
    @if (session('xoa'))
        <div class="alert alert-success">
            {{ session('xoa') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Phiếu mượn </div>
    </div>
    <div class="row">
        {{-- <div class="col-sm-4 col-md-3">
            <div class="input-group mb-3">
                
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                    </button>
                </div>
            </div>
        </div> --}}
        <div class="col-sm-6 col-md-6 "></div>
        <div class="col-sm-6 col-md-6 d-flex justify-content-end gap-3 mb-2">
            <button class="btn btn-primary" type="button"><i class="fas fa-plus-circle"><a
                        href="{{ route('getAddCallCard') }}" class="text-white text-decoration-none"><span
                            class="ml-2">Thêm</span></i></a></button>
        </div>
        {{-- <div class="col-sm-6 col-md-3">
            <div class="input-group mb-4">
                <select class="form-select" id="inputGroupSelect03">
                    <option value="">Chọn người mượn</option>
                    @foreach ($listStudent as $Student)
                        <option value="{{ $Student->id_student }}">{{ $Student->name_student }}</option>
                    @endforeach
                </select>
            </div>
        </div> --}}

        <div class="table-responsive">
            <table class="table table-bordered table-striped col-xs-6">
                <thead>
                    <tr class="table-active" style="text-align: center">
                        <th scope="col" class="col-md-2">Mã phiếu mượn</th>
                        <th scope="col">Người mượn </th>
                        <th scope="col">Ngày mượn</th>
                        <th scope="col">Hạn trả</th>
                        <th scope="col">Số sách mượn</th>
                        <th scope="col">Đã trả</th>
                        <th scope="col">Chi tiết</th>
                        <th scope="col">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listCallCard as $callCard)
                        <tr style="text-align: center" id = "tBody">
                            <td>{{ $callCard->id_call_card }}</td>
                            <td>{{ $callCard->student->name_student }}</td>
                            <td>{{ date('d-m-Y', strtotime($callCard->date_call_card)) }}</td>
                            <td>{{ date('d-m-Y', strtotime($callCard->deadline)) }}</td>
                            <td>{{ $callCard->detailCallCard->count('id_call_card') }}</td>
                            <td>{{ ($listDetailCallCard->where('id_call_card',$callCard->id_call_card)->whereNotNull('id_pay_card')->count('id_detail_call_card')) }}</td>
                            <td><a href="{{ route('loadDetailCallCard', ['id_CallCard' => $callCard->id_call_card]) }}"><i
                                        class="fas fa-info"></i></td>
                            <td><a href="{{ route('deleteCallCard', ['id_CallCard' => $callCard->id_call_card]) }}"
                                    onclick="return confirm('Bạn có chắc chắn muốn phiếu mượn sách này?');"><i
                                        class="fas fa-trash"><span class="ml-2"
                                            id="btnDelete">Xóa</span></i></a>
                            </td>
                        </tr>
                </tbody>
                @endforeach
            </table>

            {!! $listCallCard->links() !!}

        </div>
    @endsection
