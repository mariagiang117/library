@extends('dashboard/admin')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Thêm sách </div>
    </div>
    <form method="POST" action="{{ route('postAddBook') }}">
        @csrf
        <div class="col-sm-6 col-md-4">
            <label for="idBook" style="font-weight:bold">Mã sách</label>
            <input type="text" name="idBook" class="form-control" placeholder="Nhập mã sách" id="idBook"
                value="{{ $id_book_lastest + 1 }}">
            @if ($errors->has('idBook'))
                <span class="invalidfeedback">
                    {{ $errors->first('idBook') }}
                </span>
            @endif
        </div>
        <div class="col-sm-6 col-md-4">
            <label for="nameBook" style="font-weight:bold">Tên sách </label>
            <select class="form-select" id="nameBook" name="nameBook" class="form-select">
                <option value="" selected>Chọn đầu sách</option>
                @foreach ($listTitleBook as $TitleBook)
                    <option value="{{ $TitleBook->id_title_book }}">{{ $TitleBook->name_title_book }}</option>
                @endforeach
                @if ($errors->has('nameBook'))
                    <span class="invalidfeedback">
                        {{ $errors->first('nameBook') }}
                    </span>
                @endif
            </select>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-danger"><a href="{{ route('loadBook') }}"
                    class="text-white text-decoration-none">Hủy</button>
            <button type="submit" class="btn btn-primary">Lưu</button>
        </div>

    </form>
@endsection
