<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailCallCard extends Model
{
    use HasFactory;
    protected $table = 'detail_call_cards';
    protected $primaryKey = 'id_detail_call_card';
    protected $fillable = [
        'id_detail_call_card',
        'id_call_card',
        'id_book',
        'status',
        'id_pay_card',
    ];
    public function getStatusAttribute($value)
    {
        if($value == 1)
        {
            return 'Đã trả';
        }
        else
        {
            return 'Chưa trả';
        }
    }
    public function callCard()
    {
        return $this->belongsTo(CallCard::class, 'id_call_card', 'id_call_card');
    }
    public function book()
    {
        return $this->belongsTo(Book::class, 'id_book', 'id_book');
    }
}
