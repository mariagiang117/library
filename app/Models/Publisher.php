<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    use HasFactory;
    protected $table = 'publishers';
    protected $primaryKey = 'id_publisher';
    protected $fillable = [
        'id_publisher',
        'name_publisher',
    ];
    public $timestamps = false;
    public function titleBooks()
    {
        return $this->hasMany(TitleBook::class, 'id_publisher', 'id_publisher');
    }
}
