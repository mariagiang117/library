<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TitleBook extends Model
{
    use HasFactory;
    protected $table = 'title_books';
    protected $primaryKey = 'id_title_book';
    protected $fillable = [
        'name_title_book',
        'page_number',
        'quantity',
        'id_category',
        'id_title_book',
        'id_shelf_book',
        'id_publisher',
        'cost',
        'image',
    ];
    public $timestamps = false;
    public function book()
    {
        return $this->hasMany(Book::class, 'id_title_book', 'id_title_book');
    }
    public function category()
    {
        return $this->belongsTo(Category::class, 'id_category', 'id_category');
    }
    public function shelfBook()
    {
        return $this->belongsTo(ShelfBook::class, 'id_shelf_book', 'id_shelf_book');
    }
    public function publisher()
    {
        return $this->belongsTo(Publisher::class, 'id_publisher', 'id_publisher');
    }
    public function authors()
    {
        return $this->belongsToMany(Author::class, 'author_titlebook', 'id_title_book', 'id_author');
    }
    
}
