<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestPayCard;
use App\Models\Book;
use App\Models\CallCard;
use App\Models\DetailCallCard;
use App\Models\PayCard;
use App\Models\Student;
use Illuminate\Http\Request;

class PayCardController extends Controller
{
    public function loadPayCard()
    {
        $list_book = Book::all();
        $callCard = CallCard::all();
        $student = Student::all();
        $listPayCard = PayCard::query()->with('book', 'detailCallCard')->paginate(5);
        return view('pay-card.loadPayCard', compact('listPayCard', 'student', 'callCard','list_book'));
    }
    public function addPayCard()
    {
        $payCard = PayCard::orderBy('id_pay_card', 'DESC')->first();
        if ($payCard == null) {
            $id_payCard_lastest = 1;
        } else {
            $id_payCard_lastest = $payCard->id_pay_card + 1;
        }

        $listPayBook = Book::where('status', 0)->get();
        return view('pay-card.addPayCard', compact('id_payCard_lastest', 'listPayBook'));
    }
    public function postAddPayCard(RequestPayCard $request)
    {
        // dd($request->all());
        $id_pay_card = $request->id_payCard;
        $date_pay_card = $request->date_pay;
        $id_book = $request->idBook;
        $status = $request->status;

        $description = '';
        $detailCallCard = DetailCallCard::where('id_book', $id_book)->where('status', 0)->with('callCard')->first();
        if (strtotime($date_pay_card) > strtotime($detailCallCard->callCard->deadline)) {
            $description = "Trả muộn";
        }
        PayCard::create([
            'id_pay_card' => $id_pay_card,
            'date_pay_card' => $date_pay_card,
            'id_book' => $id_book,
            'status' => $status,
            'description' => $description,
            'id_call_card' => $detailCallCard->id_call_card,
            'id_detail_call_card' => $detailCallCard->id_detail_call_card,
        ]);
        $detailCallCard->update([
            'status' => 1,
            'id_pay_card' => $id_pay_card,
        ]);
        Book::find($id_book)->update([
            'status' => $status,
        ]);

        return redirect()->route('loadPayCard')->with('them', 'Thêm thành công');
    }

    public function getUpdatePayCard(PayCard $id_PayCard)
    {
        $listPayBook = Book::where('status', 0)->get();
        return view('pay-card.updatePayCard', compact('id_PayCard', 'listPayBook'));
    }

    public function postUpdatePayCard(PayCard $id_PayCard, Request $request)
    {
        $date_pay = $request->date_pay;
        $status = $request->status;

        $CallCard = CallCard::find($id_PayCard->id_call_card);

        $description = '';
        if (strtotime($date_pay) > strtotime($CallCard->deadline)) {
            $description = "Trả muộn";
        }
        $id_PayCard->update([
            'date_pay_card' => $date_pay,
            'status' => $status,
            'description' => $description,
        ]);
        Book::find($id_PayCard->id_book)->update([
            'status' => $status,
        ]);

        return redirect()->route('loadPayCard')->with('sua', 'Sửa thành công');
    }
    public function deletePayCard(PayCard $id_PayCard)
    {
        $id_PayCard->delete();
        $detailCallCard = $id_PayCard->id_detail_call_card;
        DetailCallCard::find($detailCallCard)->update([
            'status' => 0,
            'id_pay_card' => null,
        ]);

        Book::find($id_PayCard->id_book)->update([
            'status' => 0,
        ]);
        return redirect()->route('loadPayCard')->with('xoa', 'Xóa thành công');
    }
}
