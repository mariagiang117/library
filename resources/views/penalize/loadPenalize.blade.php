@extends('dashboard/admin')
@section('content')
    @if (session('them'))
        <div class="alert alert-success">
            {{ session('them') }}
        </div>
    @endif
    @if (session('sua'))
        <div class="alert alert-success ">
            {{ session('sua') }}
        </div>
    @endif
    @if (session('xoa'))
        <div class="alert alert-success">
            {{ session('xoa') }}
        </div>
    @endif
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Phiếu bồi thường </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-6 "></div>
        <div class="col-sm-6 col-md-6 d-flex justify-content-end gap-3 mb-3">
            <button class="btn btn-primary" type="button"><i class="fas fa-plus-circle"><a
                        href="{{ route('loadPayCard') }}" class="text-white text-decoration-none"><span
                            class="ml-2">Đi tới Phiếu trả</span></i></a></button>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped col-xs-6">
                <thead>
                    <tr style="text-align: center" class="table-active">
                        <th scope="col" class="col-md-2">Mã phiếu bồi thường</th>
                        <th scope="col" class="col-md-2">Mã phiếu trả sách</th>
                        <th scope="col" class="col-md-1">Mã sách</th>
                        <th scope="col" class="col-md-1">Giá sách</th>
                        <th scope="col" class="col-md-1">Mã học sinh</th>
                        <th scope="col" class="col-md-2">Tên học sinh</th>
                        <th scope="col" class="col-md-2">Mô tả</th>
                        <th scope="col" class="col-md-2">Tình trạng</th>
                        <th scope="col">Xóa</th>
                        <th scope="col">Sửa</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listPenalize as $penalize)
                        <tr style="text-align: center">
                            <td>{{ $penalize->id }}</td>
                            <td>{{ $penalize->id_pay_card }}</td>
                            <td>{{ $penalize->payCard->id_book }}</td>
                            <td>{{ $title_book->find($book->find($penalize->payCard->id_book)->id_title_book)->cost }}
                            </td>
                            <td>{{ $callCard->find($penalize->payCard->id_call_card)->id_student }}</td>
                            <td>{{ $student->find($callCard->find($penalize->payCard->id_call_card)->id_student)->name_student }}
                            </td>
                            <td>{{ $penalize->description }}</td>
                            <td>{{ $penalize->status }}</td>
                            <td><a href="{{ route('deletePenalize', ['id_Penalize' => $penalize->id]) }}"
                                    onclick="return confirm('Bạn có chắc chắn muốn phiếu bồi thường này?');"><i
                                        class="fas fa-trash"><span class="ml-2"
                                            id="btnDelete">Xóa</span></i></a>
                            </td>
                            <td><a href="{{ route('getUpdatePenalize', ['id_Penalize' => $penalize->id]) }}"><i
                                        class="fas fa-edit"><span class="ml-2">Sửa</span></i></a></td>

                        </tr>
                </tbody>
                @endforeach
            </table>

            {!! $listPenalize->links() !!}

        </div>
    @endsection
