<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\CallCard;
use App\Models\Category;
use App\Models\TitleBook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\DetailCallCard;

class HomeController extends Controller
{
    public function getHome()
    {
        $user = Auth::user();
        $student = $user->student;
        $query = "";
        $listTitleBook = DB::table('title_books')
            ->join('categories', 'title_books.id_category', '=', 'categories.id_category')
            ->join('shelf_books', 'title_books.id_shelf_book', '=', 'shelf_books.id_shelf_book')
            ->join('publishers', 'title_books.id_publisher', '=', 'publishers.id_publisher')
            ->join('author_titlebook', 'title_books.id_title_book', '=', 'author_titlebook.id_title_book')
            ->join('authors', 'author_titlebook.id_author', '=', 'authors.id_author')
            ->where('title_books.name_title_book', 'LIKE', '%' . $query . '%')
            ->orwhere('categories.name_category', 'LIKE', '%' . $query . '%')
            ->orwhere('shelf_books.name_shelf_book', 'LIKE', '%' . $query . '%')
            ->orwhere('publishers.name_publisher', 'LIKE', '%' . $query . '%')
            ->orWhere('authors.name_author', 'LIKE', '%' . $query . '%')
            ->select('title_books.*', 'categories.name_category', 'shelf_books.name_shelf_book', 'publishers.name_publisher', 'authors.name_author')
            ->paginate(8);
        $listCategory = Category::all();
        return view('trangchu.library', compact('listTitleBook', 'student', 'listCategory'));
    }
    public function getSachMuon(Request $request)
    {
        $user = Auth::user();
        $student = $user->student;
        $id_student = $student->id_student;
        $listCategory = Category::all();
        $listDetailCallCard = DB::table('detail_call_cards')->where('detail_call_cards.status', 0)
            ->join('call_cards', 'detail_call_cards.id_call_card', '=', 'call_cards.id_call_card')
            ->where('id_student', $id_student)
            ->join('books', 'detail_call_cards.id_book', '=', 'books.id_book')
            ->join('title_books', 'books.id_title_book', '=', 'title_books.id_title_book')
            ->join('author_titlebook', 'title_books.id_title_book', '=', 'author_titlebook.id_title_book')
            ->join('authors', 'author_titlebook.id_author', '=', 'authors.id_author')
            ->paginate(4);
        return view('trangchu.book-borrowing', compact('listDetailCallCard','listCategory','student'));
    }
}
