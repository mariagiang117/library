<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
    protected $primaryKey = 'id_book';
    protected $fillable = [
        'id_book',
        'id_title_book',
        'status',
        'description',
        
    ];
    public $timestamps = false;
    public function getStatusAttribute($value)
    {
        $status = '';
        switch ($value) {
            case '0':
                $status = 'Đang mượn';
                break;
            case '1':
                $status = 'Có sẵn';
                break;
            case '2':
                $status = 'Bị mất';
                break;
            case '3':
                $status = 'Bị rách';
                break;
        }
        return $status;
    }
    public function titleBook()
    {
        return $this->belongsTo(TitleBook::class, 'id_title_book', 'id_title_book');
    }
    public function callCards()
    {
        return $this->belongsToMany(CallCard::class, 'detail_call_cards', 'id_book', 'id_call_card');
    }
}
