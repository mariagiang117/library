<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TRƯỜNG TRUNG HỌC PHỔ THÔNG C THANH LIÊM</title>
</head>

<body>
    <div
        style="background-color: lightblue;    padding: 20px;    border-radius: 5px;    box-sizing: border-box;    margin: 0 auto;    width: 100%;">
        <h1 style="text-align: center;color:black">Thông báo về việc mượn sách đã quá hạn trả</h1>
        <div
            style="background-color: whitesmoke;        padding: 20px;        border-radius: 10px;        width: 80%;        margin: 0 auto;">
            <div style="text-align: center; font-size:16px;color:black">Gửi <b>{{ $name_student }}</b>, bạn đã mượn
                <b>thư viện THPT C Thanh Liêm </b>
                cuốn sách <b>{{ $name_book }}</b>. Hôm nay đã quá hạn trả là <b>{{ $date_expire }} ngày.</b>
                <p style="font-size:16px;color:black"> Hãy nhanh chân đến thư viện trả lại sách nhé!</p>
            </div>
        </div>
    </div>

</body>

</html>
