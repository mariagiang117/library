@extends('dashboard/admin')
@section('content')
@if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Danh sách các sách quá hạn trả </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-striped col-xs-6" >
                <thead>
                    <tr style="text-align: center" class="table-active">
                        <th scope="col" class="">STT</th>
                        <th scope="col" class="col-md-1">Mã sách</th>
                        <th scope="col" class="col-md-1">Mã học sinh</th>
                        <th scope="col" class="col-md-2">Tên học sinh</th>
                        <th scope="col" class="col-md-2">Ngày mượn</th>
                        <th scope="col" class="col-md-2">Hạn trả</th>
                        <th scope="col" class="col-md-2">Số ngày quá hạn</th>
                        <th scope="col" class="col-md-2">Thông báo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listExpireBook as $expireBook)
                        <tr style="text-align: center">
                            <td>{{ $stt++ }}</td>
                            <td>{{ $expireBook->id_book }}</td>
                            <td>{{ $expireBook->callCard->id_student }}</td>
                            <td>{{ $student->find($expireBook->callCard->id_student)->name_student }}</td>
                            <td>{{ date('d-m-Y', strtotime($expireBook->callCard->date_call_card)) }}</td>
                            <td>{{ date('d-m-Y', strtotime($expireBook->callCard->deadline)) }}</td>
                            <td>{{ $expireBook->day_expire }} ngày</td>
                            <td> <a
                                href="{{ route('mailforExpire', ['id_detail' => $expireBook->id_detail_call_card]) }}"><button type="button" class="btn btn-warning">
                                    Gửi Mail</button></a></td>
                </tbody>
                @endforeach
            </table>

            {{-- {!! $listExpireBook->links() !!} --}}

        </div>
    @endsection
