// change color of login button
var email = document.getElementById('exampleInputEmail');
var password = document.getElementById('exampleInputPassword');
var btnLogin = document.querySelector('.btnLogin');
email.addEventListener('input', changeColor);
password.addEventListener('input', changeColor);
function changeColor() {
    if (email.value !== '' && password.value !== '') {
        btnLogin.classList.add('btnLogin_change');
        btnLogin.removeAttribute('disabled');
    }else {
        btnLogin.classList.remove('btnLogin_change'); 
        btnLogin.setAttribute('disabled', true);
    }
}