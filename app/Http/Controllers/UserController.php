<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User as ModelsUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Foundation\Auth\User as AuthUser;

class UserController extends Controller
{
    public function getChangePassword()
    {
        return view('profile.change-pass');
    }

    public function postChangePassword(Request $request)
    {
        $user = Auth::user();
        $present_password = $user->password;
        
        $old_password = $request->old_pass;
        $new_password = $request->new_pass;
        $confirm_password = $request->re_pass;
       
        if ($new_password == $confirm_password) {
            if (Hash::check($old_password, $present_password)) {
                
                ModelsUser::find(auth()->user()->id)->update(['password'=> Hash::make($new_password)]);

                return redirect()->back()->with('success', 'Đổi mật khẩu thành công');
            } else {
                return redirect()->back()->with('error', 'Mật khẩu cũ không đúng');
            }
        } else {
            return redirect()->back()->with('error', 'Mật khẩu mới không trùng khớp');
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
