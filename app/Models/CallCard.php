<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CallCard extends Model
{
    use HasFactory;
    protected $table = 'call_cards';
    protected $primaryKey = 'id_call_card';
    protected $fillable = [
        'id_call_card',
        'id_student',
        'date_call_card',
        'deadline',
    ];
    public function student()
    {
        return $this->belongsTo(Student::class,'id_student','id_student');
    }
    public function books()
    {
        return $this->belongsToMany(Book::class, 'detail_call_cards', 'id_call_card', 'id_book');
    }
    public function detailCallCard()
    {
        return $this->hasMany(DetailCallCard::class,'id_call_card','id_call_card');
    }
}
