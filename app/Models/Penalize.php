<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penalize extends Model
{
    use HasFactory;
    protected $table = 'penalize';
    protected $fillable = [
        'id_pay_card',
        'status',
        'description',
    ];
    public $timestamps = false;
    public function getStatusAttribute($value)
    {
        if($value == 1)
        {
            return 'Đã bồi thường';
        }
        else
        {
            return 'Chưa bồi thường';
        }
    }
    public function payCard()
    {
        return $this->hasOne(PayCard::class, 'id_pay_card', 'id_pay_card');
    }
}
