<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CallCard;
use App\Models\DetailCallCard;
use App\Models\Book;
use App\Models\Student;
use App\Models\TitleBook;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Response;

class DetailCallCardController extends Controller
{
    public function loadDetailCallCard(CallCard $id_CallCard)
    {
        $listStudent = Student::all();
        $listCallBook = Book::where('status', 1)->get();
        $listDetailCallCard = DetailCallCard::where('id_call_card', $id_CallCard->id_call_card)
            ->with('book')->paginate(5);
        return view(
            'detail-callCard.loadDetailCallCard',
            compact('id_CallCard', 'listDetailCallCard', 'listCallBook', 'listStudent')
        );
    }
    public function addDetailCallCard(Request $request)
    {
        if ($request->ajax()) {
            $id_call_card = $request->id_callCard;
            $id_book = $request->id_book;
            $list_detail = DB::table('detail_call_cards')->where('id_call_card', $id_call_card)->select('id_book')->get()->toArray();
            $arr_book = [];
            foreach ($list_detail as $book) {
                $arr_book[] = $book->id_book;
            }
            $data = '';
            if (count($list_detail) !== 0) {
                if (in_array($id_book, $arr_book)) {
                    $data = "Sách đã tồn tại trong danh sách";
                    echo json_encode($data);
                } else {
                    DetailCallCard::create([
                        'id_call_card' => $id_call_card,
                        'id_book' => $id_book,
                    ]);
                }
            } else {
                DetailCallCard::create([
                    'id_call_card' => $id_call_card,
                    'id_book' => $id_book,
                ]);
            }
            $detail = DB::table('detail_call_cards')->orderByDesc('id_detail_call_card')
                ->join('books', 'detail_call_cards.id_book', '=', 'books.id_book')
                ->join('title_books', 'books.id_title_book', '=', 'title_books.id_title_book')
                ->select('detail_call_cards.*', 'title_books.name_title_book')->first();
            return json_encode($detail);
        }
    }
    public function deleteDetailCallCard(DetailCallCard $id_DetailCallCard)
    {
        $id_DetailCallCard->delete();
        return json_encode((array('statusCode' => '200')));
    }

    public function deleteListDetail($id_CallCard)
    {
        $listDetail = DetailCallCard::where('id_call_card', $id_CallCard)->get();
        foreach ($listDetail as $detail) {
            $detail->delete();
        }
        return redirect()->route('loadCallCard');
    }
}
