<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthorTitleBook extends Model
{
    use HasFactory;
    protected $table = 'author_titleBook';
    protected $primaryKey = 'id_author_titleBook';
    protected $fillable = [
        'id_author_titleBook',
        'id_author',
        'id_title_book',
    ];
    public $timestamps = false;
}
