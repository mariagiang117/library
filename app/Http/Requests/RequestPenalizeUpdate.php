<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestPenalizeUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'status' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'description.required' => 'Vui lòng nhập lý do bồi thường',
            'status.required' => 'Vui lòng chọn trạng thái',
        ];
    }
}
