@extends('dashboard/admin')
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Danh sách các sách đã bị mất, hỏng </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-6 "></div>
        <div class="col-sm-6 col-md-6 d-flex justify-content-end gap-3 mb-3">
            <button class="btn btn-primary" type="button"><i class="fas fa-download"><a
                        href="{{route('exportLostBook')}}" class="text-white text-decoration-none"><span
                            class="ml-2">Xuất file</span></i></a></button>

        </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-striped col-xs-6" >
                <thead>
                    <tr style="text-align: center" class="table-active">
                        <th scope="col" class="col-md-1">STT</th>
                        <th scope="col" class="col-md-1">Mã sách</th>
                        <th scope="col" class="col-md-2">Tên sách</th>
                        <th scope="col" class="col-md-2">Tình trạng</th>
                        <th scope="col" class="col-md-2">Người làm hỏng</th>
                        <th scope="col" class="col-md-2">Bồi thường</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listPayCard as $payCard)
                        <tr style="text-align: center">
                            <td>{{ $stt++ }}</td>
                            <td>{{ $payCard->id_book }}</td>
                            <td>{{ $titleBook->find($book->find($payCard->id_book)->id_title_book)->name_title_book }}
                            </td>
                            <td>{{ $payCard->status }}</td>
                            <td>{{ $payCard->callCard->id_student }}_{{ $student->find($payCard->callCard->id_student)->name_student }}
                            </td>
                            <td>
                                @if ($payCard->penalize == null)
                                    {{ 'Chưa bồi thường' }}
                                @else
                                    {{ $payCard->penalize->description }}
                                @endif
                            </td>
                </tbody>
                @endforeach
            </table>

            {!! $listPayCard->links() !!}

        </div>
    @endsection
