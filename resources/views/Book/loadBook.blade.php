@extends('dashboard/admin')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    @if (session('them'))
        <div class="alert alert-success">
            {{ session('them') }}
        </div>
    @endif
    @if (session('sua'))
        <div class="alert alert-success ">
            {{ session('sua') }}
        </div>
    @endif
    @if (session('xoa'))
        <div class="alert alert-success">
            {{ session('xoa') }}
        </div>
    @endif
    <div class="d-sm-flex align-items-center justify-content-between ">
        <div class="h4 mb-0">Sách </div>
    </div>
    <div class="row">
        {{-- <div class="col-sm-4 col-md-3">
            <div class="input-group mb-3">
                <input type="text" class="form-control bg-light border-0 small " placeholder="Nhập mã sách..."
                    aria-label="Search" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                    </button>
                </div>
            </div>
        </div> --}}
        <div class="col-sm-6 col-md-6 "></div>
        <div class="col-sm-6 col-md-6 d-flex justify-content-end gap-3 ">
            <button class="btn btn-primary" type="button"><i class="fas fa-plus-circle"><a href="{{ route('addBook') }}"
                        class="text-white text-decoration-none"><span class="ml-2">Thêm</span></i></a></button>

        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="input-group mb-3">
                    <select class="form-select" id="inputGroupSelect03" aria-label="Example select with button addon">
                        <option value="">Chọn đầu sách</option>
                        @foreach ($listTitleBook as $titleBook)
                            <option value="{{ $titleBook->id_title_book }}">{{ $titleBook->name_title_book }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive" style="margin-top: 5px">
        <table class="table  table-striped col-xs-6">
            <thead style="text-align: center">
                <tr class="table-active">
                    <th class="col-md-1">Mã sách</th>
                    <th>Tên sách</th>
                    <th>Tình trạng</th>
                    <th>Mô tả</th>
                    <th>Xóa</th>
                    <th>Sửa</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listBook as $book)
                    <tr style="text-align: center">
                        <td>{{ $book->id_book }}</td>
                        <td>{{ $book->titleBook->name_title_book }}</td>
                        <td>{{ $book->status }}</td>
                        <td>{{ $book->description }}</td>
                        <td><a href="{{ route('deleteBook', ['id_book' => $book->id_book]) }}"
                                onclick="return confirm('Bạn có chắc chắn muốn xóa sách này?');"><i
                                    class="fas fa-trash"><span class="ml-2" id="btnDelete">Xóa</span></i></a>
                        </td>
                        <td><a href="{{ route('getUpdateBook', ['id_book' => $book->id_book]) }}"><i
                                    class="fas fa-edit"><span class="ml-2">Sửa</span></i></a></td>
                    </tr>
            </tbody>
            @endforeach
        </table>
    </div>
    {!! $listBook->links() !!}
@endsection
