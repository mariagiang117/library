<div id="titleBook">
    <table class="table table-striped col-xs-6">
        <thead>
            <tr style="text-align: center" class="table-active">
                <th scope="col" class="col-md-1">Mã đầu sách</th>
                <th scope="col">Tên đầu sách</th>
                <th scope="col">Tác giả</th>
                <th scope="col">Thể loại</th>
                <th scope="col">Nhà xuất bản</th>
                <th scope="col">Kệ sách</th>
                <th scope="col">Số lượng</th>
                <th scope="col">Xóa</th>
                <th scope="col">Sửa</th>
            </tr>
        </thead>
        <tbody id="tbody">
            @foreach ($listTitleBook as $titleBook)
                <tr style="text-align: center">
                    <td>{{ $titleBook->id_title_book }}</td>
                    <td>{{ $titleBook->name_title_book }}</td>
                    <td>
                        {{ $titleBook->name_author }}
                    </td>
                    <td>{{ $titleBook->name_category }}</td>
                    <td>{{ $titleBook->name_publisher }}</td>
                    <td>{{ $titleBook->name_shelf_book }}</td>
                    <td>{{ $titleBook->quantity }}</td>
                    <td><a href="{{ route('deleteTitleBook', ['id_TitleBook' => $titleBook->id_title_book]) }}"
                            onclick="return confirm('Bạn có chắc chắn muốn xóa sách này?');"><i
                                class="fas fa-trash"><span class="ml-2" id="btnDelete">Xóa</span></i></a>
                    </td>
                    <td><a href="{{ route('getUpdateTitleBook', ['id_TitleBook' => $titleBook->id_title_book]) }}"><i
                                class="fas fa-edit"><span class="ml-2">Sửa</span></i></a></td>
                </tr>
        </tbody>
        @endforeach
    </table>
    <nav aria-label="Page navigation" id="pagination">
        {!! $listTitleBook->links() !!}
    </nav>
</div>
<script>
    $(document).ready(function() {
        $('#pagination a').on('click', function(e) {
            e.preventDefault();
            var author = $('#id_author').val();
            var publisher = $('#id_publisher').val();
            var category = $('#id_category').val();
            var shelfBook = $('#id_shelf').val();
            var url = $(this).attr('href');
            console.log(url);
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                data: {
                    author: author,
                    publisher: publisher,
                    category: category,
                    shelfBook: shelfBook,
                },
                success: function(data) {
                    $('#titleBook').html('');
                    $('#titleBook').html(data);
                }
            });
        });
    })
</script>
