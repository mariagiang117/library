@extends('dashboard/admin')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    @if (session('them'))
        <div class="alert alert-success">
            {{ session('them') }}
        </div>
    @endif
    @if (session('sua'))
        <div class="alert alert-success ">
            {{ session('sua') }}
        </div>
    @endif
    @if (session('xoa'))
        <div class="alert alert-success">
            {{ session('xoa') }}
        </div>
    @endif
    <div class="h4 mb-0">Đầu sách</div>

    <div class="row">
        <div class="col-sm-6 col-md-6 "></div>
        <div class="col-sm-6 col-md-6 d-flex justify-content-end gap-3 mb-3">
            <button class="btn btn-primary" type="button"><i class="fas fa-plus-circle"><a
                        href="{{ route('getAddTitleBook') }}" class="text-white text-decoration-none"><span
                            class="ml-2">Thêm</span></i></a></button>

        </div>
    </div>
    {{-- <div class="col-sm-4 col-md-4">
        <div class="input-group mb-3">
            <input type="text" id="ipSearch" name="search" class="form-control bg-light border-1 small "
                placeholder="Tìm kiếm..." aria-label="Search" aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button" id="btnSearch">
                    <i class="fas fa-search fa-sm"></i>
                </button>
            </div>
        </div>
    </div> --}}
    <div class="row">
        <div class="col-sm-6 col-md-4">
            <div class="input-group mb-3">
                <select class="form-select" id="id_author" aria-label="Example select with button addon">
                    <option value="">--Chọn tác giả--</option>
                    @foreach ($listAuthor as $Author)
                        <option value="{{ $Author->id_author }}">{{ $Author->name_author }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-sm-6 col-md-4">
            <div class="input-group mb-3">
                <select class="form-select" id="id_publisher" aria-label="Example select with button addon">
                    <option value="">--Chọn nhà xuất bản--</option>
                    @foreach ($listPublisher as $Publisher)
                        <option value="{{ $Publisher->id_publisher }}">{{ $Publisher->name_publisher }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="input-group mb-3">
                <select class="form-select" id="id_category" aria-label="Example select with button addon">
                    <option value=""> --Chọn thể loại--</option>
                    @foreach ($listCategory as $Category)
                        <option value="{{ $Category->id_category }}">{{ $Category->name_category }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="input-group mb-3">
                <select class="form-select" id="id_shelf" aria-label="Example select with button addon">
                    <option value="">--Chọn kệ sách--</option>
                    @foreach ($listShelfBook as $ShelfBook)
                        <option value="{{ $ShelfBook->id_shelf_book }}">{{ $ShelfBook->name_shelf_book }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-4 col-md-5">
            <div class="input-group mb-3">
                <input type="text" id="nameTitlebook" name="nameTitlebook" class="form-control bg-light border-1 small "
                    placeholder="Nhập tên sách..." aria-label="Search" aria-describedby="basic-addon2">
            </div>
        </div>
        <div class="col-sm-6 col-md-2 d-flex justify-content-end gap-3 mb-3">
            <button class="btn btn-primary btnSearch" type="button"> <i class="fas fa-search fa-sm">
                    <span class="ml-2">Tìm kiếm</span></i></button>

        </div>

    </div>
    <div class="table-responsive" style="margin-top: 5px" id="listTitleBook">
        <table class="table table-striped col-xs-6" id="titleBook">
            <thead>
                <tr style="text-align: center" class="table-active">
                    <th scope="col" class="col-md-1">Mã đầu sách</th>
                    <th scope="col">Tên đầu sách</th>
                    <th scope="col">Tác giả</th>
                    <th scope="col">Thể loại</th>
                    <th scope="col">Nhà xuất bản</th>
                    <th scope="col">Kệ sách</th>
                    <th scope="col">Số lượng</th>
                    <th scope="col">Xóa</th>
                    <th scope="col">Sửa</th>
                </tr>
            </thead>
            <tbody id="tbody">
                @foreach ($listTitleBook as $titleBook)
                    <tr style="text-align: center">
                        <td>{{ $titleBook->id_title_book }}</td>
                        <td>{{ $titleBook->name_title_book }}</td>
                        <td>
                            @foreach ($titleBook->authors as $authors)
                                <div>{{ $authors->name_author }}</div>
                            @endforeach
                        </td>
                        <td>{{ $titleBook->category->name_category }}</td>
                        <td>{{ $titleBook->publisher->name_publisher }}</td>
                        <td>{{ $titleBook->shelfBook->name_shelf_book }}</td>
                        <td>{{ $titleBook->quantity }}</td>
                        <td><a href="{{ route('deleteTitleBook', ['id_TitleBook' => $titleBook->id_title_book]) }}"
                                onclick="return confirm('Bạn có chắc chắn muốn xóa sách này?');"><i
                                    class="fas fa-trash"><span class="ml-2" id="btnDelete">Xóa</span></i></a>
                        </td>
                        <td><a href="{{ route('getUpdateTitleBook', ['id_TitleBook' => $titleBook->id_title_book]) }}"><i
                                    class="fas fa-edit"><span class="ml-2">Sửa</span></i></a></td>
                    </tr>
            </tbody>
            @endforeach
        </table>
        {!! $listTitleBook->links() !!}
    </div>
@endsection

@section('scripts')
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> --}}
    <script>
        $(document).ready(function() {
            $('.btnSearch').click(function() {
                var author = $('#id_author').val();
                var publisher = $('#id_publisher').val();
                var category = $('#id_category').val();
                var shelfBook = $('#id_shelf').val();
                var nameTitlebook = $('#nameTitlebook').val();
                $.ajax({
                    url: "{{ route('searchTitleBook') }}",
                    method: "GET",
                    dataType: "json",
                    data: {
                        author: author,
                        publisher: publisher,
                        category: category,
                        shelfBook: shelfBook,
                        nameTitlebook: nameTitlebook,
                    },
                    success: function(data) {
                        $("#listTitleBook").html('');
                        $('#listTitleBook').html(data);
                    }
                })
            })
        });
    </script>
@endsection
