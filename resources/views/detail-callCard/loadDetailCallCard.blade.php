@extends('dashboard/admin')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Chi tiết phiếu mượn </div>
    </div>
    <form class="user" method="POST" action="{{route('postUpdateCallCard',['id_CallCard' => $id_CallCard->id_call_card])}}">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="mb-3 row">
                <label for="id_callCard" class="col-sm-2 col-form-label" style="font-weight:bold">Mã phiếu mượn</label>
                <div class="col-sm-6 col-md-3">
                    <input type="text" name="id_callCard" class="form-control" placeholder="Nhập mã sách" id="id_callCard"
                        value="{{ $id_CallCard->id_call_card }}" readonly>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="id_student" class="col-sm-2 col-form-label" style="font-weight:bold">Người mượn</label>
                <div class="col-sm-6 col-md-3">
                    <select name="id_student" id="id_student" class="form-select">
                        <option value="{{$id_CallCard->id_student}}" selected>{{ $id_CallCard->student->name_student }}</option>
                        @foreach ($listStudent as $student)
                            <option value="{{$student->id_student}}">{{$student->name_student}}</option>
                        @endforeach
                    </select>
                    
                </div>
            </div>
            <div class="mb-3 row">
                <label for="date_call" class="col-sm-2 col-form-label" style="font-weight:bold">Ngày mượn</label>
                <div class="col-sm-6 col-md-3">
                    <input type="date" name="date_call" class="form-control" id="date_call"
                        value="{{ $id_CallCard->date_call_card }}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="deadline" class="col-sm-2 col-form-label" style="font-weight:bold">Hạn trả</label>
                <div class="col-sm-6 col-md-3">
                    <input type="date" name="deadline" class="form-control" id="deadline"
                        value="{{ $id_CallCard->deadline }}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="" class="col-sm-2 col-form-label" style="font-weight:bold">Các sách mượn</label>

            </div>
        </div>

        <div class="table">

            <table class=" table-striped ">
                <thead >
                    <tr style="text-align: center" class="table-active">
                        <th scope="col" class="col-md-2">Mã sách</th>
                        <th scope="col" class="">Tình trạng </th>
                        <th scope="col" class="">Mã phiếu trả </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listDetailCallCard as $detailCallCard)
                        <tr style="text-align: center">
                            <td class="col-md-2">{{ $detailCallCard->book->id_book }}</td>
                            <td class="col-md-4">{{ $detailCallCard->status }}</td>
                            <td class="col-md-4">{{ $detailCallCard->id_pay_card }}</td>
                        </tr>
                </tbody>
                @endforeach
            </table>
            {!! $listDetailCallCard->links() !!}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn  btn-danger"><a href="{{ route('loadCallCard') }}"
                    class="text-white text-decoration-none">Đóng</button>
                    <button type="submit" class="btn btn-primary">Lưu</button>
        </div>
    </form>
@endsection
