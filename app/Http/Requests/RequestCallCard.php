<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestCallCard extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'id_call_card' => 'required|numeric|unique:call_cards,id_call_card',
            'id_student' => 'required',
            'date_call' => 'required',
            'deadline' => 'required',

        ];
    }
    public function messages()
    {
        return [
            // 'id_call_card.required' => 'Vui lòng nhập mã phiếu mượn',
            // 'id_call_card.numeric' => 'Mã phiếu mượn phải là số',
            // 'id_call_card.unique' => 'Mã lịch hẹn đã tồn tại',
            'id_student.required' => 'Vui lòng chọn học sinh',
            'date_call.required' => 'Vui lòng chọn ngày mượn',
            'deadline.required' => 'Vui lòng chọn hạn trả',
        ];
    }
}
