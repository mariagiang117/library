<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function loadCategory()
    {
    }
    public function addCategory()
    {
    }
    public function updateCategory($id_category, Request $request)
    {
    }
    public function deleteCategory($id_category)
    {
    }
    public function titleBooks()
    {
        return $this->hasMany(TitleBook::class,'id_category');
    }
}
