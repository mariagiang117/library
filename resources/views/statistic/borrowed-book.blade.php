@extends('dashboard/admin')
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Danh sách các sách đã mượn </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-striped col-xs-6">
                <thead>
                    <tr style="text-align: center" class="table-active">
                        <th scope="col" class="col-md-1">STT</th>
                        <th scope="col" class="col-md-1">Mã sách</th>
                        <th scope="col" class="col-md-3">Tên sách</th>
                        <th scope="col" class="col-md-2">Số lượt mượn</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listDetailCallCard as $detailCallCard)
                        <tr style="text-align: center">
                            <td>{{ $stt++ }}</td>
                            <td>{{ $detailCallCard->id_book }}</td>
                            <td>{{ $titleBook->find($book->find($detailCallCard->id_book)->id_title_book)->name_title_book }}
                            </td>
                            <td>{{ $detailCallCard->total }}</td>
                </tbody>
                @endforeach
            </table>

            {!! $listDetailCallCard->links() !!}

        </div>
    @endsection
