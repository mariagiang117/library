<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TRƯỜNG TRUNG HỌC PHỔ THÔNG C THANH LIÊM</title>
</head>

<body>
    <div
        style="background-color: lightblue;    padding: 20px;    border-radius: 5px;    box-sizing: border-box;    margin: 0 auto;    width: 100%;">
        <h1 style="text-align: center">Mail lấy lại mật khẩu!</h1>
        <div
            style="background-color: whitesmoke;        padding: 20px;        border-radius: 10px;        width: 80%;        margin: 0 auto;">
            <div style="text-align: center"> Gửi <b>{{ $user }}</b>, chúng tôi vừa nhận được yêu cầu lấy lại mật
                    khẩu của bạn.
                </div>
            <div style="text-align: center">Đây là mật khẩu mới: <b>{{ $data }}</b></div>
        </div>
    </div>

</body>

</html>
