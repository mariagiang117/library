<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayCard extends Model
{
    use HasFactory;
    protected $table = 'pay_cards';
    protected $primaryKey = 'id_pay_card';
    protected $fillable = [
        'id_pay_card',
        'date_pay_card',
        'id_book',
        'status',
        'description',
        'id_call_card',
        'id_detail_call_card',
    ];
    public function getStatusAttribute($value)
    {
        $status = '';
        switch ($value) {
            case '1':
                $status = 'Đã trả';
                break;
            case '2':
                $status = 'Bị mất';
                break;
            case '3':
                $status = 'Bị rách';
                break;
        }
        return $status;
    }
    public function book()
    {
        return $this->belongsTo(Book::class, 'id_book', 'id_book');
    }
    public function detailCallCard()
    {
        return $this->hasOne(DetailCallCard::class, 'id_detail_call_card', 'id_detail_call_card');
    }
    public function penalize()
    {
        return $this->hasOne(Penalize::class, 'id_pay_card', 'id_pay_card');
    }
    public function callCard()
    {
        return $this->belongsTo(CallCard::class, 'id_call_card', 'id_call_card');
    }
}
