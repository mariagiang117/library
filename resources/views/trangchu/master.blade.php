<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Thư viện trường THPT C Thanh Liêm</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Animate.css -->
    <link rel="stylesheet" href="/css/homes/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="/css/homes/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="/css/homes/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="/css/homes/magnific-popup.css">

    <!-- Flexslider  -->
    <link rel="stylesheet" href="/css/homes/flexslider.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="/css/homes/style.css">
    <link rel="stylesheet" href="/css/homes/custome.css">
</head>

<body>
    <!--header -->

    <div class="colorlib-loader"></div>
    <div id="page">
        <nav class="colorlib-nav" role="navigation">
            <div class="top-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-2">
                            <div id="colorlib-logo"><a href=""><img src="/images/logo12.png" alt="logo"
                                        style="width: 400px;height: 40px;"></a></div>
                        </div>
                        <div class="col-xs-10 text-right menu-1">
                            <ul>
                                <li class=""><a href="{{route('trangchu')}}">Trang chủ</a></li>
                                <li class="has-dropdown">
                                    <a href="">Tài nguyên</a>

                                    <ul class="dropdown">
                                        @foreach ($listCategory as $category)
                                            <li><a href="">{{ $category->name_category }}</a></li>
                                        @endforeach

                                    </ul>

                                </li>
                               
                                <li class="has-dropdown">
                                    <a href=""><b>{{ $student->name_student }}</b></a>
                                    <ul class="dropdown">
                                        <li><a href="{{route('logout')}}">Đăng xuất</a></li>
                                        <li><a href="">Tài khoản</a></li>
										<li><a href="{{route('book-borrowing')}}">Sách đang mượn</a></li>
                                    </ul>
                                </li>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <aside id="colorlib-hero">
            <div class="flexslider">
                <ul class="slides">
                    <li style="background-image: url(/images/1.jpg);">
                        <div class="overlay"></div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-md-pull-2 col-sm-12 col-xs-12 slider-text">

                                </div>
                            </div>
                        </div>
                    </li>
                    <li style="background-image: url(/images/book/truong1.jpg);">
                        <div class="overlay"></div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-md-pull-2 col-sm-12 col-xs-12 slider-text">

                                </div>
                            </div>
                        </div>
                    </li>
                    <li style="background-image: url(/images/book/truong2.jpg);">
                        <div class="overlay"></div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 col-md-push-3 col-sm-12 col-xs-12 slider-text">

                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </aside>
        <!-- End header -->
        <!-- main -->
        <div class="sidebar">
            @yield('sidebar')
        </div>
        <div class="content">
            @yield('content')
        </div>

        <!-- end main -->
        <!-- footer -->
        <footer id="colorlib-footer" role="contentinfo" style="background-color:bisque ">
            <div class="container">
                <div class="row row-pb-md">
                    <div class="col-md-6 colorlib-widget">
                        <h4>Giới thiệu</h4>
                        <p>Trường THPT C Thanh Liêm</p>
                        <p>    ĐỊA CHỈ: Xã Thanh Thủy - Huyện Thanh Liêm - Tỉnh Hà Nam
                           </p>
                        <p>
                        <ul class="colorlib-social-icons">

                            <li><a href=""><i class="icon-facebook"></i></a></li>

                            <li><a href=""><i
                                        class="icon-youtube"></i></a></li>
                        </ul>
                        </p>
                    </div>
                    <div class="col-md-6">
                        <h4>Thông tin liên hệ</h4>
                        <ul class="colorlib-footer-links">
                            <li>Địa Chỉ: Xã Thanh Thủy, Huyện Thanh Liêm, Tỉnh Hà Nam</li>
                            <li><a href="tel://1234567920">SĐT: 0226.388.6289</a></li>
                            <li><a href="mailto:c3thanhliemc.hanam@moet.edu.vn">Email:
                                    c3thanhliemc.hanam@moet.edu.vn</a></li>
                            <li><a href="http://hanam.edu.vn/thptcthanhliem">Website:
                                    http://hanam.edu.vn/thptcthanhliem</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </footer>
        <!--end  footer -->
    </div>


    <!-- jQuery -->
    <script src="/js/trangchu/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="/js/trangchu/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="/js/trangchu/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="/js/trangchu/jquery.waypoints.min.js"></script>
    <!-- Flexslider -->
    <script src="/js/trangchu/jquery.flexslider-min.js"></script>

    <script src="/js/trangchu/owl.carousel.min.js"></script>
    <!-- Magnific Popup -->
    <script src="/js/trangchu/jquery.magnific-popup.min.js"></script>
    <script src="/js/trangchu/magnific-popup-options.js"></script>

    <!-- Stellar Parallax -->
    <script src="/js/trangchu/jquery.stellar.min.js"></script>
    <!-- Main -->
    <script src="/js/trangchu/main.js"></script>

</body>

</html>
