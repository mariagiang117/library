@extends('dashboard/admin')
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Danh sách các sách đang mượn </div>
    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-striped col-xs-6">
                <thead style="text-align: center" class="table-active">
                    <tr >
                        <th scope="col" class="col-md-1">STT</th>
                        <th scope="col" class="col-md-1">Mã sách</th>
                        <th scope="col" class="col-md-2">Tên sách</th>
                        <th scope="col" class="col-md-2">Mã học sinh</th>
                        <th scope="col" class="col-md-2">Tên học sinh</th>
                        <th scope="col" class="col-md-2">Ngày mượn</th>
                        <th scope="col" class="col-md-2">Hạn trả</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($listDetailCallCard as $detailCallCard)
                        <tr style="text-align: center">
                            <td>{{ $stt++ }}</td>
                            <td>{{ $detailCallCard->id_book }}</td>
                            <td>{{ $titleBook->find($book->find($detailCallCard->id_book)->id_title_book)->name_title_book }}
                            </td>
                            <td>{{ $detailCallCard->callCard->id_student }}</td>
                            <td>{{ $student->find($detailCallCard->callCard->id_student)->name_student }}</td>
                            <td>{{ date('d-m-Y', strtotime($detailCallCard->callCard->date_call_card)) }}</td>
                            <td>{{ date('d-m-Y', strtotime($detailCallCard->callCard->deadline)) }}</td>
                </tbody>
                @endforeach
            </table>

            {!! $listDetailCallCard->links() !!}

        </div>
    @endsection
