<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaidToPayCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pay_cards', function (Blueprint $table) {
            $table->integer('status')->default(1);
            $table->text('description')->nullable();
            $table->integer('id_call_card')->nullable();
            $table->integer('id_detail_call_card')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pay_cards', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('description');
            $table->dropColumn('id_call_card');
            $table->dropColumn('id_detail_call_card');
        });
    }
}
