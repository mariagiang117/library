<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificationAboutOverdueBook extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name_student;
    public $name_book;
    public $date_expire;
    public function __construct($name_student, $name_book, $date_expire)
    {
        $this->name_student = $name_student;
        $this->name_book = $name_book;
        $this->date_expire = $date_expire;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('viewMail.MailForExpire'); 
    }
}
