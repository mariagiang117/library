<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\NotificationAboutOverdueBook;

class MailExpireBook implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $name_student;
    public $name_book;
    public $date_expire;
    public $email;
    public function __construct( $email,$name_student, $name_book, $date_expire)
    {
        $this->name_student = $name_student;
        $this->name_book = $name_book;
        $this->date_expire = $date_expire;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->email)->send(new NotificationAboutOverdueBook($this->name_student, $this->name_book, $this->date_expire));
    }
}
