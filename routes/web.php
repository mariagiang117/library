<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\TitleBookController;
use App\Http\Controllers\PaycardController;
use App\Http\Controllers\CallCardController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ReaderController;
use App\Http\Controllers\DetailCallCardController;
use App\Http\Controllers\PenalizeController;
use App\Http\Controllers\StatisticController;
use Illuminate\Support\Facades\Route;
use PHPUnit\TextUI\XmlConfiguration\Group;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//admin
Route::prefix('admin')->group(function () {
    Route::middleware('auth')->group(function () {
        Route::get('/', [AdminController::class, 'getIndex'])->name('admin.index');

        //quản lý thể loại
        Route::get('loadCategory', [CategoryController::class, 'loadCategory']);
        Route::post('addCategory', [CategoryController::class, 'addCategory'])->name('addCategory');
        Route::post('updateCategory/{id_category}', [CategoryController::class, 'updateCategory'])->name('updateCategory');
        Route::get('deleteCategory/{id_category}', [CategoryController::class, 'deleteCategory'])->name('deleteCategory');

        //quản lý sách
        Route::get('loadBook', [BookController::class, 'loadBook'])->name('loadBook');
        Route::get('addBook', [BookController::class, 'addBook'])->name('addBook');
        Route::post('addBook', [BookController::class, 'postAddBook'])->name('postAddBook');
        Route::get('updateBook/{id_book}', [BookController::class, 'getUpdateBook'])->name('getUpdateBook');
        Route::put('updateBook/{id_book}', [BookController::class, 'postUpdateBook'])->name('postUpdateBook');
        Route::get('deleteBook/{id_book}', [BookController::class, 'deleteBook'])->name('deleteBook');

        //quản lý các đầu sách TitleBook
        Route::get('loadTitleBook', [TitleBookController::class, 'loadTitleBook'])->name('loadTitleBook');
        Route::get('addTitleBook', [TitleBookController::class, 'addTitleBook'])->name('getAddTitleBook');
        Route::post('addTitleBook', [TitleBookController::class, 'postAddTitleBook'])->name('postAddTitleBook');
        Route::get('updateTitleBook/{id_TitleBook}', [TitleBookController::class, 'updateTitleBook'])->name('getUpdateTitleBook');
        Route::put('updateTitleBook/{id_TitleBook}', [TitleBookController::class, 'postUpdateTitleBook'])->name('postUpdateTitleBook');
        Route::get('deleteTitleBook/{id_TitleBook}', [TitleBookController::class, 'deleteTitleBook'])->name('deleteTitleBook');
        ROute::get('searchTitleBook', [TitleBookController::class, 'searchTitleBook'])->name('searchTitleBook');
        //quản lý bạn đọc
        Route::get('loadReader', [ReaderController::class, 'loadReader']);
        Route::get('addReader', [ReaderController::class, 'addReader'])->name('viewAddReader');
        Route::post('addReader', [ReaderController::class, 'postAddReader'])->name('postAddReader');
        Route::get('updateReader/{id_reader}', [ReaderController::class, 'updateReader'])->name('viewUpdateReader');
        Route::post('updateReader/{id_reader}', [ReaderController::class, 'postUpdateReader'])->name('postUpdateReader');
        Route::post('deleteReader/{id_reader}', [ReaderController::class, 'deleteReader'])->name('deleteReader');

        //quản lý phiếu mượn
        Route::get('loadCallCard', [CallCardController::class, 'loadCallCard'])->name('loadCallCard');
        Route::get('addCallCard', [CallCardController::class, 'addCallCard'])->name('getAddCallCard');
        Route::post('addCallCard', [CallCardController::class, 'postAddCallCard'])->name('postAddCallCard');
        Route::put('updateCallCard/{id_CallCard}', [CallCardController::class, 'postUpdateCallCard'])->name('postUpdateCallCard');
        Route::get('deleteCallCard/{id_CallCard}', [CallCardController::class, 'deleteCallCard'])->name('deleteCallCard');

        //quản lý chi tiết phiếu mượn
        Route::get('loadDetailCallCard/{id_CallCard}', [DetailCallCardController::class, 'loadDetailCallCard'])->name('loadDetailCallCard');
        
        Route::post('addDetailCallCard', [DetailCallCardController::class, 'addDetailCallCard'])->name('addDetailCallCard');
        
        Route::get('deleteDetailCallCard/{id_DetailCallCard}', [DetailCallCardController::class, 'deleteDetailCallCard'])->name('deleteDetailCallCard');
        
        Route::get('deleteListDetail/{id_CallCard}', [DetailCallCardController::class, 'deleteListDetail'])->name('deleteListDetail');
        //quản lý phiếu trả
        Route::get('loadPayCard', [PayCardController::class, 'loadPayCard'])->name('loadPayCard');
        Route::get('addPayCard', [PayCardController::class, 'addPayCard'])->name('getAddPayCard');
        Route::post('addPayCard', [PayCardController::class, 'postAddPayCard'])->name('postAddPayCard');
        Route::get('updatePayCard/{id_PayCard}', [PayCardController::class, 'getUpdatePayCard'])->name('getUpdatePayCard');
        Route::put('updatePayCard/{id_PayCard}', [PayCardController::class, 'postUpdatePayCard'])->name('postUpdatePayCard');
        Route::get('deletePayCard/{id_PayCard}', [PayCardController::class, 'deletePayCard'])->name('deletePayCard');

        //bồi thường
        Route::get('loadPenalize', [PenalizeController::class, 'loadPenalize'])->name('loadPenalize');
        Route::get('addPenalize/{id_PayCard}', [PenalizeController::class, 'addPenalize'])->name('getAddPenalize');
        Route::post('addPenalize/{id_PayCard}', [PenalizeController::class, 'postAddPenalize'])->name('postAddPenalize');
        Route::get('updatePenalize/{id_Penalize}', [PenalizeController::class, 'updatePenalize'])->name('getUpdatePenalize');
        Route::put('updatePenalize/{id_Penalize}', [PenalizeController::class, 'postUpdatePenalize'])->name('postUpdatePenalize');
        Route::get('deletePenalize/{id_Penalize}', [PenalizeController::class, 'deletePenalize'])->name('deletePenalize');

        // thống kê
        Route::get('expireBook', [StatisticController::class, 'expireBook'])->name('expireBook');
        Route::get('mailforExpire/{id_detail}', [StatisticController::class, 'mailforExpire'])->name('mailforExpire');
        Route::get('bookBorrowing', [StatisticController::class, 'bookBorrowing'])->name('bookBorrowing');
        Route::get('borrowedBook',[StatisticController::class, 'borrowedBook'])->name('borrowedBook');
        Route::get('lostBook',[StatisticController::class, 'lostBook'])->name('lostBook');
        Route::get('exportLostBook',[StatisticController::class, 'exportLostBook'])->name('exportLostBook');
    });
});
//trang chủ
Route::prefix('library')->group(function () {
    Route::get('login', [LoginController::class, 'getLogin'])->name('login');
    Route::post('login', [LoginController::class, 'postLogin'])->name('post_login');
    Route::get('forgotPassword', [LoginController::class, 'getForgotPassword']);
    Route::post('forgotPassword', [LoginController::class, 'postForgotPassword'])->name('post_forgot_password');
    Route::middleware('auth')->group(function () {
        Route::get('profile', [UserController::class, 'getProfile']);
        Route::post('profile', [UserController::class, 'postProfile']);
        Route::get('changePassword', [UserController::class, 'getChangePassword'])->name('changePassword');
        Route::post('changePassword', [UserController::class, 'postChangePassword'])->name('postChangePassword');
        Route::get('logout', [UserController::class, 'getLogout'])->name('logout');
        Route::get('home', [HomeController::class, 'getHome'])->name('trangchu');
       Route::get('book-borrowing', [HomeController::class, 'getSachMuon'])->name('book-borrowing');
    });
});
