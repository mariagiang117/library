<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestPenalize;
use App\Http\Requests\RequestPenalizeUpdate;
use App\Models\CallCard;
use App\Models\PayCard;
use App\Models\Penalize;
use App\Models\Book;
use App\Models\Student;
use App\Models\TitleBook;
use Illuminate\Http\Request;

class PenalizeController extends Controller
{
    public function loadPenalize()
    {
        $student = Student::all();
        $callCard = CallCard::all();
        $title_book = TitleBook::all();
        $book = Book::all();
        $listPenalize = Penalize::query()->with('payCard')->paginate(10);
         return view('penalize.loadPenalize',compact('listPenalize','student','callCard','title_book','book'));
    }
    public function addPenalize(PayCard $id_PayCard)
    {
        $id_penalize = Penalize::orderBy('id', 'DESC')->first();
        if ($id_penalize == null) {
            $id_penalize = 1;
        } else {
            $id_penalize = $id_penalize->id + 1;
        }

        $idStudent = CallCard::find($id_PayCard->id_call_card)->id_student;
        
        $name_student = Student::find($idStudent)->name_student;

        $id_title_book = Book::find($id_PayCard->id_book)->id_title_book;

        $name_book = TitleBook::find($id_title_book)->name_title_book;

        $cost = TitleBook::find($id_title_book)->cost;

        return view('penalize.addPenalize', compact('id_penalize', 'id_PayCard','idStudent','name_student','name_book','cost'));
    }
    public function postAddPenalize(PayCard $id_PayCard,RequestPenalize $request)
    {
        Penalize::create([
            'id' =>$request->id_penalize,
            'description' => $request->description,
            'status' => $request->status,
            'id_pay_card' => $id_PayCard->id_pay_card,
        ]);
        return redirect()->route('loadPenalize')->with('them', 'Thêm mới thành công');
    }
    public function updatePenalize(Penalize $id_Penalize)
    {
        return view('penalize.updatePenalize', compact('id_Penalize'));
    }
    public function postUpdatePenalize(Penalize $id_Penalize,RequestPenalizeUpdate $request)
    {
        // dd($request->all());
        $id_Penalize->update([
            'description' => $request->description,
            'status' => $request->status,
        ]);
        return redirect()->route('loadPenalize')->with('sua', 'Sửa thành công');
    }
    public function deletePenalize(Penalize $id_Penalize)
    {
        $id_Penalize->delete();
        return redirect()->route('loadPenalize')->with('xoa', 'Xóa thành công');
    }
}
