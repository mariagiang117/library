@extends('dashboard/admin')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0">Thêm phiếu trả </div>
    </div>
    <form method="POST" action="{{ route('postAddPayCard') }}">
        @csrf
        <div class="row">
            <div class="mb-3 row">
                <label for="id_payCard" class="col-sm-2 col-form-label" style="font-weight:bold">Mã phiếu trả</label>
                <div class="col-sm-6 col-md-3">
                    <input type="text" name="id_payCard" class="form-control" placeholder="Nhập mã sách" id="id_payCard"
                        value="{{ $id_payCard_lastest }}">
                
                @if ($errors->has('id_payCard'))
                    <span class="invalidfeedback">
                        {{ $errors->first('id_payCard') }}
                    </span>
                @endif
            </div>
            </div>
            <div class="mb-3 row">
                <label for="date_pay" class="col-sm-2 col-form-label" style="font-weight:bold">Ngày trả</label>
                <div class="col-sm-6 col-md-3">
                    <input type="date" name="date_pay" class="form-control" id="date_pay">
                
                @if ($errors->has('date_pay'))
                    <span class="invalidfeedback">
                        {{ $errors->first('date_pay') }}
                    </span>
                @endif
            </div>
            </div>
            <div class="mb-3 row">
                <label for="idBook" class="col-sm-2 col-form-label" style="font-weight:bold">Sách trả</label>
                <div class="col-sm-6 col-md-3">
                    <select class="form-select" id="idBook" name="idBook">
                        <option selected disabled>Chọn sách trả</option>
                        @foreach ($listPayBook as $book)
                            <option value="{{ $book->id_book }}">{{ $book->id_book }} _
                                {{ $book->titleBook->name_title_book }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('idBook'))
                        <span class="invalidfeedback">
                            {{ $errors->first('idBook') }}
                        </span>
                    @endif
                </div>
            </div>

            <div class="mb-3 row">
                <label for="status" class="col-sm-2 col-form-label" style="font-weight:bold">Tình trạng</label>
                <div class="col-sm-6 col-md-3">
                    <select class="form-select" id="status" name="status">
                        <option selected value="1">Đã trả</option>
                        <option value="2">Bị mất</option>
                        <option value="3">Bị rách</option>
                    </select>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger"><a href="{{ route('loadPayCard') }}"
                        class="text-white text-decoration-none">Hủy</button>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    @endsection
