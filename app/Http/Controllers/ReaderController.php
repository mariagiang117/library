<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestReader;

class ReaderController extends Controller
{
    public function loadReader()
    {
        return view('Reader/loadReader');
    }
    public function addReader()
    {
        return view('Reader/addReader');
    }
    public function postAddReader(RequestReader $request)
    {
        
    }
    public function updateReader($id_Reader,RequestReader $request)
    {
        return view('Reader/updateReader');
    }
    public function postUpdateReader($id_Reader,RequestReader $request)
    {

    }
    public function deleteReader($id_Reader)
    {

    }
}
