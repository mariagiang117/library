<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestTitleBook extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idTitleBook' => 'required|numeric|unique:title_books,id_title_book',
            'nameTitleBook' => 'required|max:255',
            'pageNumber' => 'required|numeric',
            'category' => 'required',
            'publisher' => 'required',
            'author' => 'required',
            'shelfBook' => 'required',
            'cost' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg',
        ];
    }
    public function messages()
    {
        return [
            'idTitleBook.required' => 'Vui lòng nhập mã sách',
            'idTitleBook.numeric' => 'Mã sách phải là số',
            'idTitleBook.unique' => 'Mã sách đã tồn tại',
            'nameTitleBook.required' => 'Tên sách không được để trống',
            'nameTitleBook.max' => 'Tên sách không được vượt quá 255 ký tự',
            'pageNumber.required' => 'Số trang không được để trống',
            'pageNumber.numeric' => 'Số trang phải là một số',
            'category.required' => 'Vui lòng chọn thể loại',
            'publisher.required' => 'Vui lòng chọn nhà xuất bản',
            'author.required' => 'Vui lòng chọn tác giả',
            'shelfBook.required' => 'Vui lòng chọn kệ sách',
            'cost.required' => 'Vui lòng nhập giá',
            'image.required' => 'Vui lòng chọn ảnh',
            'image.mimes' => 'Ảnh phải có định dạng jpeg,png,jpg',
        ];
    }
}
