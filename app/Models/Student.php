<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $table = 'students';
    protected $primaryKey = 'id_student';
    protected $fillable = [
        'id_student',
        'name_student',
        'class',
        'sex',
        'date_birth',
        'address',
        'email',
    ];
    public $timestamps = false;
    public function haveCallCard()
    {
        return $this->hasMany(CallCard::class,'id_student','id_student');
    }
    public function havePayCard()
    {
        return $this->hasMany(PayCard::class,'id_student','id_student');
    }
}
