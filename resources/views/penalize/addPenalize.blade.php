@extends('dashboard/admin')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0">Tạo phiếu bồi thường </div>
    </div>
    <form method="POST" action="{{ route('postAddPenalize', ['id_PayCard' => $id_PayCard->id_pay_card]) }}">
        @csrf
        <div class="row">
            <div class="mb-3 row">
                <label for="id_penalize" class="col-sm-2 col-form-label" style="font-weight:bold">Mã phiếu bồi thường</label>
                <div class="col-sm-6 col-md-3">
                    <input type="text" name="id_penalize" class="form-control" placeholder="Nhập mã sách" id="id_penalize"
                        value="{{ $id_penalize }}">
                    @if ($errors->has('id_penalize'))
                        <span class="invalidfeedback">
                            {{ $errors->first('id_penalize') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="id_pay_card" class="col-sm-2 col-form-label" style="font-weight:bold">Mã phiếu trả</label>
                <div class="col-sm-6 col-md-3">
                    <input type="text" name="id_pay_card" class="form-control" placeholder="Nhập mã sách" id="id_pay_card"
                        value="{{ $id_PayCard->id_pay_card }}" readonly>
                    @if ($errors->has('id_pay_card'))
                        <span class="invalidfeedback">
                            {{ $errors->first('id_pay_card') }}
                        </span>
                    @endif
                </div>
            </div>

            <div class="mb-3 row">
                <label for="idBook" class="col-sm-2 col-form-label" style="font-weight:bold">Sách đã mượn</label>
                <div class="col-sm-6 col-md-3">
                    <input type="text" name="idBook" class="form-control" id="idBook"
                        value="{{ $id_PayCard->id_book }}_{{ $name_book }}" readonly>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="cost" class="col-sm-2 col-form-label" style="font-weight:bold">Giá sách</label>
                <div class="col-sm-6 col-md-3">
                    <input type="text" name="cost" class="form-control" id="cost" value="{{ $cost }}" readonly>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="idStudent" class="col-sm-2 col-form-label" style="font-weight:bold">Học sinh</label>
                <div class="col-sm-6 col-md-3">
                    <input type="text" name="idStudent" class="form-control" id="idStudent"
                        value="{{ $idStudent }}_{{ $name_student }}" readonly>
                </div>
            </div>
            <div class="mb-3 row">
                <label for="description" class="col-sm-2 col-form-label" style="font-weight:bold">Mô tả</label>
                <div class="col-sm-6 col-md-3">
                    <input type="text" name="description" class="form-control" id="description">
                    @if ($errors->has('description'))
                        <span class="invalidfeedback">
                            {{ $errors->first('description') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <label for="status" class="col-sm-2 col-form-label" style="font-weight:bold">Tình trạng</label>
                <div class="col-sm-6 col-md-3">
                    <select class="form-select" id="status" name="status">
                        <option selected disabled>Chọn tình trạng</option>
                        <option value="0">Chưa bồi thường</option>
                        <option value="1">Đã bồi thường</option>
                    </select>
                    @if ($errors->has('status'))
                        <span class="invalidfeedback">
                            {{ $errors->first('status') }}
                        </span>
                    @endif
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger"><a href="{{ route('loadPenalize') }}"
                        class="text-white text-decoration-none">Hủy</button>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div>
        </div>
    @endsection
