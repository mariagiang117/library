@extends('trangchu/master')
@section('content')
    <div class="colorlib-shop">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center colorlib-heading">
                    <h2><span>Sách đang mượn</span></h2>

                </div>
            </div>
            <div class="row">
                @foreach ($listDetailCallCard as $book)
                    <div class="col-md-3 text-center">
                        <div class="product-entry">
                            <div class="product-img" style="background-image: url(/images/book/{{ $book->image }});">
                                <div class="cart">
                                    <p>
                                        {{-- <span class="addtocart"><a href="cart.html"><i
                                                    class="icon-shopping-cart"></i></a></span> --}}
                                        <span><a href=""><i class="icon-eye"></i></a></span>
                                    </p>
                                </div>
                            </div>
                            <div class="desc">
                                <h3>{{ $book->name_title_book }}</h3>
                                <p class="price"><span>{{ $book->name_author }}</span></p>
                                <h3>Ngày mượn: {{ $book->date_call_card }}</h3>
                                <h3>Hạn trả: {{ $book->deadline }}</h3>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

   {!!$listDetailCallCard->links()!!}
@endsection
