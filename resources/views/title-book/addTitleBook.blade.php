@extends('dashboard/admin')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .invalidfeedback {
            display: block;
            font-size: 1rem;
            color: #e3342f;
        }
    </style>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h4 mb-0 ">Thêm đầu sách </div>
    </div>
    <form class="user" method="POST" action="{{ route('postAddTitleBook') }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="idTitle_Book" style="font-weight:bold">Mã đầu sách</label>
                    <input type="text" id="idTitle_Book" name="idTitleBook" placeholder="Nhập mã sách" class="form-control"
                        value="{{ $id_titleBook_lastest }}">
                </div>
                @if ($errors->has('idTitleBook'))
                    <span class="invalidfeedback">
                        {{ $errors->first('idTitleBook') }}
                    </span>
                @endif
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="nameTitle_Book" style="font-weight:bold">Tên đầu sách</label>
                    <input type="text" id="nameTitle_Book" name="nameTitleBook" placeholder="Nhập tên sách"
                        class="form-control">
                    @if ($errors->has('nameTitleBook'))
                        <span class="invalidfeedback">
                            {{ $errors->first('nameTitleBook') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="page" style="font-weight:bold">Số trang</label>
                    <input type="text" id="page" name="pageNumber" placeholder="Nhập số trang" class="form-control">
                    @if ($errors->has('pageNumber'))
                        <span class="invalidfeedback">
                            {{ $errors->first('pageNumber') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <label for="cost" style="font-weight:bold">Giá sách</label>
                <input type="text" id="cost" name="cost" placeholder="Nhâp giá sách" class="form-control">
                @if ($errors->has('cost'))
                    <span class="invalidfeedback">
                        {{ $errors->first('cost') }}
                    </span>
                @endif
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="categories" style="font-weight:bold">Thể loại</label>
                    <select id="categories" name="category" class="form-select">
                        <option selected disabled>Chọn thể loại</option>
                        @foreach ($listCategory as $Category)
                            <option value="{{ $Category->id_category }}">{{ $Category->name_category }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('category'))
                        <div class="invalidfeedback">
                            {{ $errors->first('category') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="publishers" style="font-weight:bold">Nhà xuất bản</label>
                    <select id="publishers" name="publisher" class="form-select">
                        <option selected disabled>Chọn nhà xuất bản</option>
                        @foreach ($listPublisher as $Publisher)
                            <option value="{{ $Publisher->id_publisher }}">{{ $Publisher->name_publisher }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('publisher'))
                        <span class="invalidfeedback">
                            {{ $errors->first('publisher') }}
                        </span>
                    @endif

                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="author" style="font-weight:bold">Tác giả </label>
                    <select id="author" name="author" class="form-select">
                        <option selected disabled>Chọn tác giả</option>
                        @foreach ($listAuthor as $Author)
                            <option value="{{ $Author->id_author }}">{{ $Author->name_author }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('author'))
                        <span class="invalidfeedback">
                            {{ $errors->first('author') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="shelfBooks" style="font-weight:bold">Kệ sách</label>
                    <select id="shelfBooks" name="shelfBook" class="form-select">
                        <option selected disabled>Chọn kệ sách</option>
                        @foreach ($listShelfBook as $ShelfBook)
                            <option value="{{ $ShelfBook->id_shelf_book }}">{{ $ShelfBook->name_shelf_book }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('shelfBook'))
                        <span class="invalidfeedback">
                            {{ $errors->first('shelfBook') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="mb-3 row">
                <div class="col-sm-6 col-md-4">
                    <label for="image" style="font-weight:bold">Chọn ảnh</label>
                    <input type="file" id="image" name="image" class="form-control">
                    {{-- @error('image')
                    <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                @enderror --}}
                    @if ($errors->has('image'))
                        <span class="invalidfeedback">
                            {{ $errors->first('image') }}
                        </span>
                    @endif
                </div>
            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-secondary"><a href="{{ route('loadTitleBook') }}"
                        class="text-white text-decoration-none">Hủy</button>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </div>
    </form>
@endsection
