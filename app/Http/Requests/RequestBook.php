<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestBook extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'idBook' => 'required|numeric|unique:books,id_book',
            'nameBook' => 'required',
            // 'statusBook' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'idBook.required' => 'Vui lòng nhập mã sách',
            'idBook.numeric' => 'Mã sách phải là số',
            'idBook.unique' => 'Mã sách đã tồn tại',
            'nameBook.required' => 'Vui lòng chọn tên sách',
            // 'statusBook.required' => 'Vui lòng chọn tình trạng sách',
        ];
    }
}
