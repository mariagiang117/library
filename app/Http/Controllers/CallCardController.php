<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestCallCard;
use App\Models\CallCard;
use App\Models\Student;
use App\Models\Book;
use App\Models\DetailCallCard;
use Illuminate\Http\Request;

class CallCardController extends Controller
{
    public function loadCallCard()
    {
        $listCallCard = CallCard::query()->with('student', 'books', 'detailCallCard')->paginate(10);

        $listStudent = Student::all();

        $listDetailCallCard = DetailCallCard::all();

        return view('call-card.loadCallCards', compact('listCallCard', 'listStudent', 'listDetailCallCard'));
    }

    public function addCallCard()
    {
        $callCard = CallCard::orderBy('id_call_card', 'DESC')->first();

        $id_callCard_lastest = $callCard->id_call_card + 1;

        $listStudent = Student::all();

        $listCallBook = Book::where('status', 1)->get();

        return view('call-card.addCallCard', compact('id_callCard_lastest', 'listStudent', 'listCallBook'));
    }

    public function postAddCallCard(RequestCallCard $request)
    {
        $list_book = DetailCallCard::where('id_call_card', $request->id_call_card)->get();
        $quantity_book = count($list_book);

        if ($quantity_book == 0) {
            return redirect()->back()->with('error', 'Vui lòng chọn sách mượn');
        } else {
            CallCard::create([
                'id_call_card' => $request->id_call_card,
                'id_student' => $request->id_student,
                'date_call_card' => $request->date_call,
                'deadline' => $request->deadline,
            ]);

            // $list_book = DetailCallCard::where('id_call_card', $request->id_call_card)->get();
            foreach ($list_book as $book) {
                Book::find($book->id_book)->update([
                    'status' => 0,
                ]);
            }
            return redirect()->route('loadCallCard')->with('them', 'Thêm thành công');
        }
    }

    public function postUpdateCallCard(CallCard $id_CallCard, RequestCallCard $request)
    {

        $id_CallCard->update([
            'id_student' => $request->id_student,
            'date_call_card' => $request->date_call,
            'deadline' => $request->deadline,
        ]);
        return redirect()->route('loadCallCard')->with('sua', 'Sửa thành công');
    }
    public function deleteCallCard(CallCard $id_CallCard)
    {
        $list_pay = $id_CallCard->detailCallCard->whereNotNull('id_pay_card')->count('id_detail_call_card');

        if ($list_pay != 0) {
            return redirect()->back()->with('error', 'Không thể xóa');
        } else {
            $list_book = DetailCallCard::where('id_call_card', $id_CallCard->id_call_card)->get();
            foreach ($list_book as $book) {
                Book::find($book->id_book)->update([
                    'status' => 1,
                ]);
            }
            foreach ($id_CallCard->detailCallCard as $detailCallCard) {
                $detailCallCard->delete();
            }
            $id_CallCard->delete();
            return redirect()->route('loadCallCard')->with('xoa', 'Xóa thành công');
        }
    }
}
