<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestPayCard extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_payCard' => 'required|numeric|unique:pay_cards,id_pay_card',
            'date_pay' => 'required',
            'idBook' => 'required',
            // 'status' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'id_payCard.required' => 'Vui lòng nhập mã phiếu trả',
            'date_pay.required' => 'Vui lòng chọn ngày trả',
            'idBook.required' => 'Vui lòng chọn sách',
            // 'status.required' => 'Vui lòng chọn trạng thái',
        ];
    }
}
